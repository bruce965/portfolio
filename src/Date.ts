/// <reference path="../typings/index.d.ts" />

export interface Date {
	year: number;
	month?: number;
	day?: number;
}

export function Date(year: number, month?: number, day?: number): Date {
	return { year: year, month: month, day: day };
}



export interface DateRange {
	start: Date;
	end: Date;
}

export function DateRange(start: Date, end: Date): DateRange {
	return { start: start, end: end };
}
