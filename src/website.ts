import { Date, DateRange } from "./Date";
import { ProjectState, Configuration } from "./Configuration";

export default <Configuration>{
	preferredScheme: "https",
	domain: "www.fabioiotti.com",
	
	contactMe: "https://7p7sugcvac.execute-api.eu-west-1.amazonaws.com/prod",
	
	tools: {
		"blender": { name: "Blender", icon: "/res/icons/blender.png", link: "//www.blender.org" },
		"unity": { name: "Unity", icon: "/res/icons/unity.png", link: "//unity3d.com" },
		"unrealengine": { name: "Unreal Engine", icon: "/res/icons/unrealengine.png", link: "//www.unrealengine.com" },
		"nodejs": { name: "Node.js", icon: "/res/icons/nodejs.png", link: "//nodejs.org" },
		"opengl": { name: "OpenGL", icon: "/res/icons/opengl.png", link: "//www.opengl.org" },
		"flash": { name: "Flash", icon: "/res/icons/flash.png", link: "//www.adobe.com/products/flashplayer" },
		"lwjgl": { name: "LWJGL (LightWeight Java Game Library)", icon: "/res/icons/lwjgl.png", link: "//legacy.lwjgl.org" },
		"irrlicht": { name: "Irrlicht Engine", icon: "/res/icons/irrlicht.png", link: "//irrlicht.sourceforge.net" },
		"protobuf": { name: "Protocol Buffers", icon: null, link: "//github.com/google/protobuf" },
		"lidgren": { name: "Lidgren Network Library", icon: null, link: "//github.com/lidgren/lidgren-network-gen3" },
		"mono": { name: "Mono", icon: "/res/icons/mono.png", link: "//www.mono-project.com" },
		"portableapps": { name: "Portable Apps", icon: "/res/icons/portableapps.png", link: "//portableapps.com" },
		"jauntepe": { name: "JauntePE", icon: "/res/icons/jauntepe.png", link: "//jauntepe.sourceforge.net" },
		
		"java": { name: "Java", icon: "/res/icons/java.png", link: "//java.com" },
		"csharp": { name: "C#", icon: "/res/icons/csharp.png", link: "//msdn.microsoft.com/library/kx37x362.aspx" },
		"typescript": { name: "TypeScript", icon: "/res/icons/typescript.png", link: "//www.typescriptlang.org" },
		"javascript": { name: "JavaScript", icon: "/res/icons/javascript.png", link: "//developer.mozilla.org/docs/Web/JavaScript" },
		"html5": { name: "HTML5", icon: "/res/icons/html5.png", link: "//developer.mozilla.org/docs/Web/Guide/HTML/HTML5" },
		"less": { name: "Less", icon: "/res/icons/less.png", link: "//lesscss.org" },
		"python": { name: "Python", icon: "/res/icons/python.png", link: "//www.python.org" },
		"as2": { name: "ActionScript 2", icon: "/res/icons/as2.png", link: "//help.adobe.com/en_US/AS2LCR/Flash_10.0/help.html" },
		"as3": { name: "ActionScript 3", icon: "/res/icons/as3.png", link: "//help.adobe.com/en_US/ActionScript/3.0_ProgrammingAS3/WS5b3ccc516d4fbf351e63e3d118a9b90204-7ff4.html" },
		
		"windows": { name: "Windows", icon: "/res/icons/windows.png", link: "//windows.microsoft.com" },
		"linux": { name: "Linux", icon: "/res/icons/tux.png", link: "//wikipedia.org/wiki/Linux" },
		"android": { name: "Android", icon: "/res/icons/android.png", link: "//www.android.com" },
		"ios": { name: "iOS", icon: "/res/icons/ios.png", link: "//www.apple.com/ios" },
		"web": { name: "Web", icon: "/res/icons/web.png", link: "//wikipedia.org/wiki/World_Wide_Web" },

		"telegram": { name: "Telegram", icon: "/res/icons/telegram.png", link: "//telegram.org" },
		"googleplus": { name: "Google Plus", icon: "/res/icons/googleplus.png", link: "//plus.google.com" },
		"facebook": { name: "Facebook", icon: "/res/icons/facebook.png", link: "//facebook.com" },
		"github": { name: "GitHub", icon: "/res/icons/github.png", link: "//github.com" },
		"bitbucket": { name: "Bitbucket", icon: "/res/icons/bitbucket.png", link: "//bitbucket.org" }
	},

	localization: {
		"en": {
			projectName: "Project Name",
			projectDescription: "Description",
			projectTools: "Tools Used",
			projectPlatforms: "Target Platforms",
			projectDate: "Date",
			projectState: "State",
			contactmeSubject: "Subject of the Message",
			contactmeMessage: "Body of the message.",
			contactmeSuccess: "Message sent successfully!",
			contactmeContentMissing: "Please write a message before submitting.",
			contactmeAjaxError: "Unexpected AJAX error, please check your connection."
		},
		"it": {
			projectName: "Nome Progetto",
			projectDescription: "Descrizione",
			projectTools: "Strumenti",
			projectPlatforms: "Piattaforme",
			projectDate: "Data",
			projectState: "Stato",
			contactmeSubject: "Oggetto del Messaggio",
			contactmeMessage: "Contenuto del messaggio.",
			contactmeSuccess: "Messaggio inviato con successo!",
			contactmeContentMissing: "Per favore scrivi un messaggio prima di inviare.",
			contactmeAjaxError: "Errore AJAX inatteso, per favore verifica la tua connessione."
		}
	},
	
	menu: [{
		path: "/",
		data: {
			"en": { title: "Fabio Iotti's Portfolio", titleShort: "Home", description: "This is my portfolio, here you can find everything you need to know about me, my projects and my ideas.", contents: "en.jsx" },
			"it": { title: "Portfolio di Fabio Iotti", titleShort: "Home", description: "Questo è il mio portfolio, qui trovi tutto quello che puoi voler sapere su di me, sui miei progetti e sulle mie idee.", contents: "it.jsx" },
		}
	}, {
		path: "/projects/",
		data: {
			"en": { title: "My Projects' List", titleShort: "Projects", description: "This is the long list with some of the projects I have worked on since I started programming.", contents: "projects/en.jsx" },
			"it": { title: "Lista dei Miei Progetti", titleShort: "Progetti", description: "Questa è la lista lunga con alcuni dei progetti su cui ho lavorato da quanto ho iniziato a programmare.", contents: "projects/it.jsx" },
		}
	}, {
		path: "/contactme/",
		data: {
			"en": { title: "Contact Me", titleShort: "Contact Me", description: "From this page you can contact me to ask me anything.", contents: "contactme/en.jsx" },
			"it": { title: "Contattami", titleShort: "Contattami", description: "Da questa pagina è possibile contattarmi per qualsiasi necessità.", contents: "contactme/it.jsx" },
		}
	}],
	
	projects: [{
		path: "/projects/openlab/",
		tools: ["blender"],
		platforms: ["windows"],
		date: Date(2009),
		state: ProjectState.completed,
		data: {
			"en": { title: "OpenLab", description: "This is my first graphical videogame ever, built with Blender logic-bricks at the age of 14.", contents: "projects/openlab/en.jsx" },
			"it": { title: "OpenLab", description: "Questo è il mio primo videogioco grafico in assoluto, creato con i logic-bricks di Blender all'età di 14 anni.", contents: "projects/openlab/it.jsx" }
		}
	}, {
		path: "/projects/3dspheresdef/",
		tools: ["blender"],
		platforms: ["windows"],
		date: Date(2010),
		state: ProjectState.completed,
		data: {
			"en": { title: "3D Spheres Defence", description: "One of my first games, made with Blender at the age of 15.", contents: "projects/3dspheresdef/en.jsx" },
			"it": { title: "3D Spheres Defence", description: "Uno dei miei primi giochi, creato con Blender a 15 anni.", contents: "projects/3dspheresdef/it.jsx" }
		}
	}, {
		path: "/projects/3dtowerdef/",
		tools: ["blender"],
		platforms: ["windows"],
		date: Date(2010),
		state: ProjectState.discontinued,
		data: {
			"en": { title: "3D Tower Defence", description: "Another game I made when I was a kid with Blender, this project was never completed.", contents: "projects/3dtowerdef/en.jsx" },
			"it": { title: "3D Tower Defence", description: "Un altro gioco che ho fatto da bambino con Blender, questo progetto non è mai stato concluso.", contents: "projects/3dtowerdef/it.jsx" }
		}
	}, {
		path: "/projects/fgassault/",
		tools: ["blender", "python"],
		platforms: ["windows"],
		date: Date(2010),
		state: ProjectState.discontinued,
		data: {
			"en": { title: "Fabiogiopla Assaulter", description: "The first game I made where I interacted with Python code, no code written from scratch but copyed and adapted.", contents: "projects/fgassault/en.jsx" },
			"it": { title: "Fabiogiopla Assaulter", description: "Il primo gioco che ho fatto in cui ho interagito con il codice Python, codice copiato ed adattato si intenda.", contents: "projects/fgassault/it.jsx" }
		}
	}, {
		path: "/projects/greenavoid/",
		tools: ["flash", "as2"],
		platforms: ["web"],
		date: Date(2011),
		state: ProjectState.completed,
		data: {
			"en": { title: "Green Avoid", description: "The first Flash game I ever published, written from scratch in ActionScript 2 at the age of 16.", contents: "projects/greenavoid/en.jsx" },
			"it": { title: "Green Avoid", description: "Il primo gioco Flash che ho pubblicato, scritto da zero in ActionScript 2 all'età di 16 anni.", contents: "projects/greenavoid/it.jsx" }
		}
	}, {
		path: "/projects/vectorwars/",
		tools: ["flash", "as3"],
		platforms: ["web"],
		date: Date(2011),
		state: ProjectState.completed,
		data: {
			"en": { title: "Vector Wars", description: "Second Flash game I published, written in ActionScript 3 at the age of 16.", contents: "projects/vectorwars/en.jsx" },
			"it": { title: "Vector Wars", description: "Il secondo gioco Flash che ho pubbliato, scritto in ActionScript 3 a 16 anni.", contents: "projects/vectorwars/it.jsx" }
		}
	}, {
		path: "/projects/spacekiller/",
		tools: ["html5", "javascript"],
		platforms: ["web"],
		date: Date(2011),
		state: ProjectState.draft,
		data: {
			"en": { title: "Space Killer", description: "My first HTML5 project, written from scratch, this is not a complete game.", contents: "projects/spacekiller/en.jsx" },
			"it": { title: "Space Killer", description: "Il mio primo progetto HTML5, scritto da zero, questo non è un gioco completo.", contents: "projects/spacekiller/it.jsx" }
		}
	}, {
		path: "/projects/sbw2d/",
		tools: ["java", "lwjgl", "opengl"],
		platforms: ["windows"],
		date: Date(2011),
		state: ProjectState.completed,
		data: {
			"en": { title: "SandBox Wars 2D", description: "After beginning to learn Java at school, this is my first playable Java game.", contents: "projects/sbw2d/en.jsx" },
			"it": { title: "SandBox Wars 2D", description: "Dopo aver cominciato a studiare Java a scuola, questo è il mio primo gioco Java giocabile.", contents: "projects/sbw2d/it.jsx" }
		}
	}, {
		path: "/projects/sbw2d2/",
		tools: ["java", "lwjgl", "opengl"],
		platforms: ["windows", "linux"],
		date: Date(2011),
		state: ProjectState.draft,
		data: {
			"en": { title: "SandBox Wars 2D (variant 2)", description: "An attempt to mimic Minecraft/Terraria I have written while learning to code in Java.", contents: "projects/sbw2d2/en.jsx" },
			"it": { title: "SandBox Wars 2D (variante 2)", description: "Un tentativo di imitare Minecraft/Terraria mentre stavo imparando il Java.", contents: "projects/sbw2d2/it.jsx" }
		}
	}, {
		path: "/projects/quickexplorer/",
		tools: ["java"],
		platforms: ["windows", "linux"],
		date: Date(2012, 2),
		state: ProjectState.completed,
		data: {
			"en": { title: "Quick Explorer", description: "Resource browser implemented in Java for the school.", contents: "projects/quickexplorer/en.jsx" },
			"it": { title: "Quick Explorer", description: "Esplora risorse implementato in Java per fini scolastici.", contents: "projects/quickexplorer/it.jsx" }
		}
	}, {
		path: "/projects/sbw3d/",
		tools: ["unity", "csharp"/*, "lidgren", "protobuf", "irrlicht", "mono"*/],
		platforms: ["windows", "linux"],
		date: Date(2012, 8),
		state: ProjectState.halted,
		data: {
			"en": { title: "SandBox Wars 3D", description: "This is the project of my life, the videogame I always wanted to write but I never find time to.", contents: "projects/sbw3d/en.jsx" },
			"it": { title: "SandBox Wars 3D", description: "Questo è il progetto della mia vita, il videogioco che da sempre voglio scrivere, ma non trovo mai il tempo per farlo.", contents: "projects/sbw3d/it.jsx" }
		}
	}, {
		path: "/projects/quickstream/",
		tools: ["java"],
		platforms: ["windows", "linux"],
		date: Date(2012, 8),
		state: ProjectState.completed,
		data: {
			"en": { title: "Quick Stream", description: "I have written this software to watch unfinished download movies.", contents: "projects/quickstream/en.jsx" },
			"it": { title: "Quick Stream", description: "Software che ho scritto per vedere film in fase di download senza aver finito di scaricarli.", contents: "projects/quickstream/it.jsx" }
		}
	}, {
		path: "/projects/erd/",
		tools: ["java", "lwjgl", "opengl"],
		platforms: ["windows"],
		date: Date(2012),
		state: ProjectState.discontinued,
		data: {
			"en": { title: "Elements Real Defense", description: "School project written in Java, models made with Blender.", contents: "projects/erd/en.jsx" },
			"it": { title: "Elements Real Defense", description: "Progetto scolastico scritto in Java, modelli realizzati con Blender.", contents: "projects/erd/it.jsx" }
		}
	}, {
		path: "/projects/fgge/",
		tools: ["html5", "javascript"],
		platforms: ["web", "android", "ios"],
		date: Date(2012),
		state: ProjectState.discontinued,
		data: {
			"en": { title: "FGGE", description: "My attempt to make a game engine in JavaScript, includes a GUI system and works on smartphones.", contents: "projects/fgge/en.jsx" },
			"it": { title: "FGGE", description: "Il mio tentativo di fare un game engine in JavaScript, include un sistema di GUI e funziona sugli smartphone.", contents: "projects/fgge/it.jsx" }
		}
	}, {
		path: "/projects/papf/",
		tools: ["portableapps", "jauntepe"],
		platforms: ["windows"],
		date: Date(2012),
		state: ProjectState.completed,
		data: {
			"en": { title: "Portable Apps Portable Platform", description: "Allows to carry applications and settings between computers.", contents: "projects/papf/en.jsx" },
			"it": { title: "Portable Apps Portable Platform", description: "Permette di trasportare programmi ed impostazioni tra più computer.", contents: "projects/papf/it.jsx" }
		}
	}],
	
	others: []
};
