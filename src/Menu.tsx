/// <reference path="../typings/index.d.ts" />

import * as React from "react";

import { Configuration, Page, LocalizedPage } from "./Configuration";
import fixLinks from "./fixLinks";
import { firstKey } from "./tools";

export interface Props {
	configuration: Configuration;
	isDefault: boolean;
	language: string;
	path: string;
}

export interface State {
	path: string;
}

function getLocalized(language: string, page: Page): LocalizedPage {
	return page.data[language] || page.data[firstKey(page.data)];
}

function getPageTitleShort(language: string, page: Page): string {
	var localized = getLocalized(language, page);
	return localized.titleShort || localized.title;
}

export default class Menu extends React.Component<Props, State> {
	
	public state: State = {
		path: this.props.path
	};
	
	protected componentDidUpdate(prevProps: Props, prevState: State, prevContext: any): void {
		if (prevProps.path != this.props.path)
			this.setState({ path: this.props.path } as State);
	}
	
	public render(): JSX.Element {
		var el = <div className="menu-container">
			<ul className="menu">
				{this.props.configuration.menu.map(page => <li key={page.path} className={"menu-item" + (this.isActive(page) ? " menu-item-active" : "")}>
					<a href={page.path} className="menu-item-link">{getPageTitleShort(this.props.language, page)}</a>
				</li>)}
			</ul>
		</div>;
		
		return fixLinks(el, this.props.configuration, this.props.isDefault, this.props.language, this.state.path || this.props.path);
	}
	
	private isActive(page: Page): boolean {
		return this.state.path == page.path || this.state.path == ("/" + this.props.language + page.path);
	}
}
