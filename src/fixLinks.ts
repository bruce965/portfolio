/// <reference path="../typings/index.d.ts" />

import * as React from "react";
import { Configuration } from "./Configuration";
import * as tools from "./tools";

export default function fixLinks(node: JSX.Element, configuration: Configuration, isDefault: boolean, language: string, path: string): JSX.Element;
export default function fixLinks(node: React.ReactNode, configuration: Configuration, isDefault: boolean, language: string, path: string): React.ReactNode;
export default function fixLinks<T>(node: React.ReactNode, configuration: Configuration, isDefault: boolean, language: string, path: string): React.ReactNode {
	if (tools.isReactFragment(node)) {
		let clone: React.ReactFragment = Array.isArray(node) ? [] : {};
		for (let key in node)
			(clone as any)[key] = fixLinks((node as any)[key], configuration, isDefault, language, path);
		
		return clone;
	}
	
	if (tools.isReactElement(node)) {
		if (node.type == "a" && node.props.href) {
			// NOTE: does not try to fix links inside links (which are not valid DOM).
			return React.cloneElement(node, { href: fixLink(node.props.href, configuration, isDefault, language, path) });
		}
		
		if (node.props.children)
			return React.cloneElement(node, { children: fixLinks(node.props.children, configuration, isDefault, language, path) })
		
		return node;
	}
	
	return node;
}

function stripQuery(url: string): string {
	var indexOfQuery = url.indexOf("?");
	var indexOfHash = url.indexOf("#");
	var minIndexOf = Math.min(indexOfQuery >= 0 ? indexOfQuery : Infinity, indexOfHash >= 0 ? indexOfHash : Infinity);
	
	if (minIndexOf >= 0)
		return url.substr(0, minIndexOf);
	
	return url;
}

export function fixLink(href: string, configuration: Configuration, isDefault: boolean, language: string, path: string): string {
	//console.log("fixLink(..., path =", path, ")");
	
	if (href.indexOf("//") == 0 || href.indexOf("://") >= 0)
		return href;

	var absolute = tools.joinURL("/", path, href);
	var absoluteNoHash = stripQuery(absolute);
	
	var pageExists = false;
	for (let page of Configuration.allPages(configuration)) {
		if (page.path == absoluteNoHash) {
			pageExists = !!page.data[language];
			break;
		}
	}
	
	if (!pageExists)
		return absolute;
	
	if (tools.isAbsoluteURL(href)) {
		if (isDefault)
			return href;
		
		return "/" + language + href;
	}
	
	var currentLocalized = isDefault ? path : ("/" + language + path);
	var absoluteLocalized = tools.joinURL("/", currentLocalized, href);
	return absoluteLocalized;
}
