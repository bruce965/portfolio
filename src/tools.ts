/// <reference path="../typings/index.d.ts" />

import * as React from "react";

// --- REACT ---

export function isReactChild(node: React.ReactNode): node is React.ReactChild {
	//type ReactChild = ReactElement<any> | ReactText;
	
	return isReactElement(node) || isReactText(node);
}

export function isReactNode(candidate: any): candidate is React.ReactNode {
	//type ReactNode = ReactChild | ReactFragment | boolean;
	
	return isReactChild(candidate) || isReactFragment(candidate) || typeof(candidate) == "boolean" || candidate == null;
}

export function isReactText(node: React.ReactNode): node is React.ReactText {
	//type ReactText = string | number;
	
	if (node == null)
		return true;

	switch (typeof node) {
	case "boolean":
	case "undefined":
	case "string":
	case "number":
		return true;
	default:
		return false;
	}
}

export function isReactFragment(node: React.ReactNode): node is React.ReactFragment {
	//// Should be Array<ReactNode> but type aliases cannot be recursive
	//type ReactFragment = {} | Array<ReactChild | any[] | boolean>;
	
	if (Array.isArray(node)) {
		for (let child of node)
			if (!isReactNode(child))
				return false;
		
		return true;
	}
	
	return false;
}

export function isReactElement(node: React.ReactNode): node is React.ReactElement<any> {
	//interface ReactElement<P> {
	//	type: string | ComponentClass<P> | SFC<P>;
	//	props: P;
	//	key?: Key;
	//}
	
	if (node == null)
		return false;

	var type = (node as any)['type'];
	return type && typeof type == "string" || typeof type == "function";
}



// --- STRINGS ---

export function startsWith(str: string, start: string): boolean {
	return str.indexOf(start) == 0;
}



// --- ARRAYS ---

export function distinct<T>(...arrays: T[][]): T[] {
	var joined: T[] = [];
	
	for (let array of arrays)
		for (let element of array)
			if (joined.indexOf(element) < 0)
				joined.push(element);
	
	return joined;
}



// --- OBJECTS ---

export function firstKey(object: { [key: string]: any }): string {
	for (let key in object)
		return key;
}

export function firstValue<T>(object: { [key: string]: T }): T {
	for (let key in object)
		return object[key];
}



// --- CSS ---

export function url(url: string): string {
	// TODO
	return url ? `url(${url})` : "none";
}



// --- URLS ---

// URL functions adapted from Node.js
// https://github.com/nodejs/node/blob/dfaa9c905530e30bd02d713c37546febb7446257/lib/path.js

// Copyright Node.js contributors. All rights reserved.

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

export function joinURL(...fragments: string[]): string {
	if (!fragments.length)
		return '.';
	
	var joined: string;
	for (let fragment of fragments) {
		if (!joined || isAbsoluteURL(fragment))
			joined = fragment;
		else
			joined += '/' + fragment;
	}
	
	if (!joined)
		return '.';
	
	return normalizeURL(joined);
}

export function normalizeURL(path: string): string {
	if (path.length === 0)
		return '.';

	const isAbsolute = isAbsoluteURL(path);
	const trailingSeparator = path[path.length - 1] == '/';

	path = normalizeURLString(path, !isAbsolute);

	if (path.length === 0 && !isAbsolute)
		path = '.';
	if (path.length > 0 && trailingSeparator)
		path += '/';

	if (isAbsolute)
		return '/' + path;
	
	return path;
}

// Resolves . and .. elements in a path with directory names
function normalizeURLString(path: string, allowAboveRoot: boolean = true): string {
	var res = '';
	var lastSlash = -1;
	var dots = 0;
	var code: number;
	
	for (var i = 0; i <= path.length; ++i) {
		if (i < path.length)
			code = path.charCodeAt(i);
		else if (code === 47)
			break;
		else
			code = 47;
		
		if (code === 47) {
			if (lastSlash === i - 1 || dots === 1) {
				// NOOP
			} else if (lastSlash !== i - 1 && dots === 2) {
				if (res.length < 2 ||
					res.charCodeAt(res.length - 1) !== 46 ||
					res.charCodeAt(res.length - 2) !== 46
				) {
					if (res.length > 2) {
						const start = res.length - 1;
						var j = start;
						for (; j >= 0; --j)
							if (res.charCodeAt(j) === 47)
								break;
						
						if (j !== start) {
							if (j === -1)
								res = '';
							else
								res = res.slice(0, j);
							
							lastSlash = i;
							dots = 0;
							continue;
						}
					} else if (res.length === 2 || res.length === 1) {
						res = '';
						lastSlash = i;
						dots = 0;
						continue;
					}
				}
				
				if (allowAboveRoot) {
					if (res.length > 0)
						res += '/..';
					else
						res = '..';
				}
			} else {
				if (res.length > 0)
					res += '/' + path.slice(lastSlash + 1, i);
				else
					res = path.slice(lastSlash + 1, i);
			}
			
			lastSlash = i;
			dots = 0;
		} else if (code === 46 && dots !== -1) {
			++dots;
		} else {
			dots = -1;
		}
	}
	
	return res;
}

export function isAbsoluteURL(url: string): boolean {
	return url[0] == '/';
}
