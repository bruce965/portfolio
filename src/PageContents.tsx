/// <reference path="../typings/index.d.ts" />

import * as React from "react";

import buildJSX from "./jsx/build";
import Menu from "./Menu";
import { Configuration, Page, LocalizedPage } from "./Configuration";
import fixLinks from "./fixLinks";

export interface Props {
	configuration: Configuration;
	isDefault: boolean;
	language: string;
	path: string;
	initialContents: React.ReactNode;
	page: LocalizedPage;
}

export interface State {
	loading: boolean;
	contents: React.ReactNode;
}

export default class PageContents extends React.Component<Props, State> {
	
	public state: State = {
		loading: false,
		contents: this.props.initialContents
	};
	
	public render(): JSX.Element {
		// NOTE: we check for `window` existence because we might not be in the browser.
		if (typeof(window) != 'undefined' && document.title != this.props.page.title)
			document.title = this.props.page.title;
		
		return <div>
			<div className="heading">
				<div className="heading-title">Fabio Iotti</div>
				<div className="heading-subtitle">Analyst Programmer</div>
			</div>
			
			<hr className="fullsize-separator" />
			<Menu configuration={this.props.configuration} isDefault={this.props.isDefault} language={this.props.language} path={this.props.path} />
			<hr className="fullsize-separator" />
			
			{this.state.loading ? "..." : fixLinks(this.state.contents, this.props.configuration, this.props.isDefault, this.props.language, this.props.path)}
			
			<hr className="fullsize-separator" />
			<div className="footer">
				<div className="footer-text">Website © 2016 Fabio Iotti.</div>
				<div className="footer-text">Released under MIT license.</div>
			</div>
		</div>;
	}
	
	protected componentDidUpdate(prevProps: Props, prevState: State, prevContext: any): void {
		if (this.props.page.contents != prevProps.page.contents) {
			this.setState({ loading: true } as State);
			$.ajax("/ajax/" + this.props.page.contents, {
				cache: true,
				dataType : "text",
				error: () => {
					// Fallback to standard page loading.
					location.replace(this.props.path);
				},
				success: data => {
					this.setState({ contents: buildJSX(data, this.props.isDefault, this. props.language, this.props.path), loading: false } as State);
				}
			});
		}
	}
}
