/// <reference path="../../typings/index.d.ts" />

import * as React from "react";

export interface Props extends React.HTMLAttributes {
	src: string;
}

export default class FlashMovie extends React.Component<Props, void> {
	
	public state: void = null;
	
	public render(): JSX.Element {
		return <object
			classID="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
			codebase="//fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,18,0"
			{...this.props}
		>
			<param name="movie" value={this.props.src} />
			<param name="quality" value="autohigh" />
			<param name="allowFullScreen" value="true" />
			<embed
				pluginspage="//www.macromedia.com/go/getflashplayer"
				quality="autohigh"
				allowFullScreen="true"
				type="application/x-shockwave-flash"
				src={this.props.src}
				{...this.props}
			/>
		</object>;
	}
}
