/// <reference path="../../typings/index.d.ts" />

import * as React from "react";

import Gallery from "./Gallery";
import ContactMeForm from "./ContactMeForm";
import ProjectsList from "./ProjectsList";
import FlashMovie from "./FlashMovie";
import KeepRatio from "./KeepRatio";
import Mail from "./Mail";
import ToolIcon from "./ToolIcon";

function evalInContext(code: string, context: { [name: string]: any }): any {
	"use strict";
	
	var argsKeys: string[] = [];
	var argsValues: any[] = [];
	for (let k in context) {
		argsKeys.push(k);
		argsValues.push(context[k]);
	}
	
	var argsAndCode = argsKeys.concat(["return " + code]);
	var func = new Function(...argsAndCode);
	return func.apply(context, argsValues);
}

export default function build(code: string, isDefault: boolean, language: string, path: string): JSX.Element {
	return evalInContext(code, {
		React: React,
		
		isDefault: isDefault,
		language: language,
		path: path,
		
		Gallery: Gallery,
		ContactMeForm: ContactMeForm,
		ProjectsList: ProjectsList,
		FlashMovie: FlashMovie,
		KeepRatio: KeepRatio,
		Mail: Mail,
		ToolIcon: ToolIcon
	}) as JSX.Element;
}
