/// <reference path="../../typings/index.d.ts" />

import * as React from "react";

import ToolIcon from "./ToolIcon";
import { ProjectState, Configuration } from "../Configuration";
import fixLinks from "../fixLinks";
import configuration from "../website";
import { firstValue } from "../tools";
import ArraySet from "../core/collections/specialized/ArraySet";
import ISet from "../core/collections/ISet";
import IEnumerable from "../core/collections/IEnumerable";
import ICollection from "../core/collections/ICollection";
import Uri from "../core/web/Uri";

export interface Props {
	isDefault: boolean;
	language: string;
	path: string;
}

export interface State {
	selectedTools: ArraySet<string>;
}

export default class ProjectList extends React.Component<Props, State> {
	
	public state: State = {
		selectedTools: ProjectList.getSelectedTools()
	};
	
	public render(): JSX.Element {
		const localization = configuration.localization[this.props.language] || firstValue(configuration.localization);
		const tools = ProjectList.getAllTools();
		
		var node = <div className="projectslist">
			<div className="projectslist-tools">{
				// TODO: sort tools alphabetically or something.
				IEnumerable.toArray(tools.map((tool, i) => <div key={tool} className="projectslist-tool">
					<input type="checkbox" defaultChecked={this.state.selectedTools.contains(tool)} onChange={e => this.setToolEnabled(tool, (e.target as HTMLInputElement).checked)} />
					<ToolIcon tool={tool} />
				</div>))
			}</div>
			<div className="projectslist-table-container">
				<table className="projectslist-table">
					<thead>
						<tr>
							<td className="projectslist-header-name">{localization.projectName}</td>
							<td className="projectslist-header-description">{localization.projectDescription}</td>
							<td className="projectslist-header-tools">{localization.projectTools}</td>
							<td className="projectslist-header-platforms">{localization.projectPlatforms}</td>
							<td className="projectslist-header-date">{localization.projectDate}</td>
							<td className="projectslist-header-state">{localization.projectState}</td>
						</tr>
					</thead>
					<tbody>
						{configuration.projects.map(project => {
							if (!ICollection.containsAny(this.state.selectedTools, project.tools))
								return null;

							const localized = project.data[this.props.language] || firstValue(project.data);
							
							return <tr key={project.path}>
								<td><a href={project.path}>{localized.titleShort || localized.title}</a></td>
								<td>{localized.description || ""}</td>
								<td><div>{project.tools.map(tool => <ToolIcon key={tool} tool={tool} />)}</div></td>
								<td><div>{project.platforms.map(tool => <ToolIcon key={tool} tool={tool} />)}</div></td>
								<td>{JSON.stringify(project.date)}</td>
								<td>{ProjectState[project.state]}</td>
							</tr>;
						})}
					</tbody>
				</table>
			</div>
		</div>;
		
		return fixLinks(node, configuration, this.props.isDefault, this.props.language, this.props.path);
	}

	private setToolEnabled(tool: string, enabled: boolean): void {
		var selectedTools = this.state.selectedTools.clone();

		if (enabled)
			selectedTools.add(tool);
		else
			selectedTools.remove(tool);
		
		this.setState({ selectedTools: selectedTools } as State);
		// TODO: update location to reflect selected tools
	}

	private static getAllTools(): ArraySet<string> {
		var tools = new ArraySet<string>();
		for (var project of configuration.projects)
			for (var tool of project.tools || [])
				tools.add(tool);
		
		return tools;
	}

	private static getSelectedTools(): ArraySet<string> {
		var tools: string[];
		if (typeof window != 'undefined') {
			var param = new Uri(window.location.href).getParameter('tools');
			if (param)
				tools = param.split(',');
		}

		return (tools && tools.length > 0) ? new ArraySet(tools) : this.getAllTools();
	}
}
