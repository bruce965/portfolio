/// <reference path="../../typings/index.d.ts" />

import * as React from "react";

import { Tool } from "../Configuration";
import configuration from "../website";
import { url } from "../tools";

export interface Props {
	tool: string;
	href?: string;
}

export interface State {
	
}

export default class ToolIcon extends React.Component<Props, State> {
	
	public state: State = {};
	
	public render(): JSX.Element {
		var tool = configuration.tools[this.props.tool];
		
		if (!tool) {
			console.warn("Unknown tool:", this.props.tool);
			tool = configuration.tools["unknown"] || {} as any;
			tool = { name: this.props.tool || tool.name, icon: tool.icon || null, link: tool.link || null };
		}
		
		return <a className="toolicon" href={this.props.href || tool.link || "#"} target="_blank" style={{ backgroundImage: url(tool.icon) }} title={tool.name} />;
	}
}
