/// <reference path="../../typings/index.d.ts" />

import * as React from "react";

export interface ImageData {
	thumbnail?: string;
	image: string;
	title?: string;
	
	children?: React.ReactNode;
}

export interface Props {
	seamless?: boolean;
	
	children: React.ReactElement<ImageData>[];
}

export interface State {
	open?: number;
}

class Gallery extends React.Component<Props, State> {
	
	public state: State = {
		open: null
	};
	
	public render(): JSX.Element {
		var imagesData = React.Children.map(this.props.children, (child: React.ReactElement<ItemProps>) => child.props);
		
		return <div className="gallery">
			{imagesData.map((imageData: ImageData, index: number) =>
				<Gallery.Item key={index} {...imageData} onClick={() => this.openImage(index)} />
			)}
			
			{this.state.open != null && <FullscreenGallery
				images={imagesData}
				open={this.state.open}
				
				seamless={this.props.seamless}
				
				onChange={index => this.setState({ open: index })}
			/>}
		</div>;
	}
	
	private openImage(index: number): void {
		this.setState({ open: index } as State);
	}
}



export interface ItemProps extends ImageData {
	onClick?(): void;
}

namespace Gallery {
	
	export class Item extends React.Component<ItemProps, void> {
		
		public state: void = null;
	
		public render(): JSX.Element {
			return <div className="gallery-item">
				<button className="gallery-item-button" onClick={e => this.props.onClick && this.props.onClick()}>
					<img className="gallery-item-image" src={this.props.thumbnail || this.props.image} alt={this.props.title || ""} title={this.props.title} />
				</button>
			</div>;
		}
	}
}



interface FullscreenGalleryProps {
	images: ImageData[];
	/** No fullscreen gallery if `null`. */
	open: number;
	
	seamless?: boolean;
	
	onChange?(index: number): void;
}

class FullscreenGallery extends React.Component<FullscreenGalleryProps, void> {
	
	public render(): JSX.Element {
		if (this.props.open == null || this.props.images.length == 0)
			return null;
		
		var imageData = this.props.images[this.props.open];
		
		return <div className="gallery-fullscreen">
			<div className="gallery-fullscreen-outer">
				<div className="gallery-fullscreen-left">
					{(this.props.seamless || this.props.open > 0) &&
						<button className="gallery-fullscreen-arrow-left" onClick={e => this.previous()}>〈</button>
					}
				</div>
				<div className="gallery-fullscreen-content" onClick={e => this.exit()}>
					<div className="gallery-fullscreen-inner">
						<div className="gallery-fullscreen-top">
							<div className="gallery-fullscreen-top-inner">
								<img
									className="gallery-fullscreen-image"
									src={imageData.image || imageData.thumbnail}
									alt={imageData.title || ""}
									title={imageData.title}
									
									onClick={e => { this.next(); e.stopPropagation(); e.preventDefault(); }}
								/>
							</div>
						</div>
						<div className="gallery-fullscreen-bottom">
							<div className="gallery-fullscreen-bottom-inner">
							{!!imageData.children &&
								<div onClick={e => e.stopPropagation()}>{imageData.children}</div>
							}
							</div>
						</div>
					</div>
				</div>
				<div className="gallery-fullscreen-right">
					{(this.props.seamless || this.props.open < (this.props.images.length - 1)) &&
						<button className="gallery-fullscreen-arrow-right" onClick={e => this.next()}>〉</button>
					}
					<button className="gallery-fullscreen-close" onClick={e => this.exit()}>❌</button>
				</div>
			</div>
		</div>;
	}
	
	protected componentDidMount(): void {
		$(window).on('keypress', this._onKeyPress);
	}
	
	protected componentWillUnmount(): void {
		$(window).off('keypress', this._onKeyPress);
	}
	
	private _onKeyPress = this.onKeyPress.bind(this);
	private onKeyPress(e: JQueryKeyEventObject): void {
		switch (e.keyCode) {
		case 27: return this.exit();  // Escape
		case 37: return this.previous();  // Left arrow
		case 39: return this.next();  // Right arrow
		}
	}
	
	private next(): void {
		var next = this.props.open + 1;
		if (next >= this.props.images.length) {
			if (!this.props.seamless)
				return;
			
			next = 0;
		}
		
		if (this.props.onChange)
			this.props.onChange(next);
	}
	
	private previous(): void {
		var previous = this.props.open - 1;
		if (previous < 0) {
			if (!this.props.seamless)
				return;
			
			previous = this.props.images.length - 1;
		}
		
		if (this.props.onChange)
			this.props.onChange(previous);
	}
	
	private exit(): void {
		if (this.props.onChange)
			this.props.onChange(null);
	}
}

export default Gallery;
