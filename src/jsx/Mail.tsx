/// <reference path="../../typings/index.d.ts" />

import * as React from "react";

export interface Props {
}

export default class Mail extends React.Component<Props, void> {
	
	public render(): JSX.Element {
		return <a href="#" target="_blank" onTouchStart={e => this.getMail(e)} onMouseMove={e => this.getMail(e)} onClick={e => this.getMail(e)}>
			<img style={{ verticalAlign: 'middle' }} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKgAAAAOCAYAAAC7DldHAAAEFElEQVRo3u3Za4hVVRQH8N/MlKllaQ+yRMmyN5WiI32IAr9VVBI9CAqkPkQhQTlYlIWVoT2GNAR7jBVBYVAQokUwHyy/lFZWGFJY9jArw7KMaSabbh9cFw6He6/7nOvIVeYPl3POPuusvV57r73WZR/a0YPfUMEMaejFkv3QvIXlDjwONb6tiLyuLad7e1yvxrW4AG34yDDKYATuwPv4HX3Ygicxftg8xXFEXKdgG34cgjlmD5Hss1vMlqdhddhwMT6NIJ2Cm7AJc/BuC8k8+1AI0jcirVd/W2P82czYLqzFmTVSfA9ex86gezyzM9dKGyNiR/kJA9iIWTm+o/ES9oTDnw8+PU3yLSNvih2OxZe4s4Gdp2FHBGwRPXvxCt7ELxH0T2Fi0P4V387NzZcid9EU34a78RX+xgZcUsIHhfW5bz9p/WQ8hy8yu251sgrmYywuw6+4p4HST+B7XIxxWIh+TM7QLI+FMg0noDvm6WmSbxl5U+zwSIxXg24ldoc8d+HneDcXrxbUsxf/4mYcgyvxXzj3xhi7DoM4t6DcRQP00bDXNbEoO7GihA8K67O/AIWjgvGFuck25Oi6sL2O0iND4Fty32zC0oyDB3B95n1HKN7TBN8y8qba4RucFfeLsT52hFOxLhOg4yNwi+jZG7tNFptzgQ7f4raCchcJ0NFxpr69zvsiPkjWp72BQufHmWpnRHh/GHBSji4f2BsxIVZYHqeHoT7MjX+A8+J+cqSKjzPvB+NMVw8pfMvIm2KHkTgx0h5cjsfwQ6T0hzO8doWji+q5Nfe8u87YuBL+S8U5GBUFYLM+SNanvcFZ4+04C3TGxB3Ym0sRIiWloq3BeCVHU0n8NpVvWXlT7DDYYM7s/czYLYrqWUkcK+O/or6rDJEPKo3aTHlMiJXWje9Csak4sgZtZ43nHfizBu3XkdZm5sZnRjummi7/yfViO3BRA+Ol8C0jb4od+vEHzojnd3B/fHsKHojx6VEEPdSEnqko4r9UbInC6NID4IPCfdA8qtXVrTg6+qMr69B2RtFxXBQd9+LpOrT9eAaLQvCxeDBSwLKg6cMLkSan4nj7/gyY2ECPFL5l5E21w6qobquFxLYoSN7DmkwhNSfzXEbPVBTxXyMszJyZ+8JOi3AVxsTiWlHCB4X7oHnsxQ3BeF60DZbVaFPAiyFoV/Bb2cDhsCAWxuo4M32OK8KpVcyPam59tGDWRv9woEm+ReVNtcMSfBIB+HJcs6jnoDJ6pqCI/4pgQQTs0igAPws7FvXBYYe26DN2Nckn5a/Zsjg7ZHwt0uCY2KVnxM6y6iDqOYwhxqxoep8UrZnuSDGTWjhAq62YeVHJ7onf5uhBTj+Ieg5jiDEqUtL2OEutq3H4bsUAbRU9Dxv8D7PTmpBTYiqmAAAAAElFTkSuQmCC" />
		</a>;
	}

	private getMail(e: React.SyntheticEvent) {
		const a = e.currentTarget as HTMLAnchorElement;
		if (a.href.indexOf('#') > -1) {
			const mail = atob("ZmFiaW9naW9wbGFAZ21haWwuY29t");
			a.href = 'mailto:' + mail;
			(a.firstChild as HTMLImageElement).alt = mail;
		}
	}
}
