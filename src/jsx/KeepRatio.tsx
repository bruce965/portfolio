/// <reference path="../../typings/index.d.ts" />

import * as React from "react";

export interface Props extends React.HTMLAttributes {
	ratio?: number;
}

export default class KeepRatio extends React.Component<Props, void> {
	
	public state: void = null;
	
	public render(): JSX.Element {
		return <div {...this.props} children={null}>
			<div style={{ width: '100%', position: 'relative' }}>
				<div style={{ width: '100%', height: '100%', position: 'absolute', overflow: 'hidden' }}>{this.props.children}</div>
				<div style={{ width: '100%', paddingTop: (100 / (this.props.ratio || 1))+'%' }} />
			</div>
		</div>;
	}
}
