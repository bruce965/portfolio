/// <reference path="../../typings/index.d.ts" />

import * as React from "react";

import { Configuration } from "../Configuration";
import configuration from "../website";
import { firstValue } from "../tools";

export interface Props {
	language: string;
}

export interface State {
	busy: boolean;
	completed: boolean;
	lastError: string;
}

export default class ContactMeForm extends React.Component<Props, State> {
	
	public state: State = {
		busy: true,
		completed: false,
		lastError: null
	};
	
	private subject: HTMLInputElement;
	private message: HTMLTextAreaElement;
	
	public render(): JSX.Element {
		const localization = configuration.localization[this.props.language] || firstValue(configuration.localization);

		return <form className="contactme" action={configuration.contactMe} method="POST" onSubmit={e => this.submit(e)}>
			<input className="contactme-subject" ref={el => this.subject = el} type="text" name="subject" placeholder={localization.contactmeSubject} />
			<textarea className="contactme-message" ref={el => this.message = el} type="text" name="message" placeholder={localization.contactmeMessage} />
			
			<button className="contactme-button" disabled={this.state.busy || this.state.completed}>Send Message</button>
			
			{this.state.completed && <div className="contactme-success">{localization.contactmeSuccess}</div>}
			
			{!!this.state.lastError && <div className="contactme-error">{this.state.lastError}</div>}
		</form>;
	}
	
	protected componentDidMount(): void {
		this.setState({ busy: false } as State);
	}
	
	private submit(e: React.FormEvent): void {
		const localization = configuration.localization[this.props.language] || firstValue(configuration.localization);

		e.preventDefault();
		
		var subject = (this.subject && this.subject.value) || "";
		var message = (this.message && this.message.value) || "";
		
		if (!subject && !message)
			return this.setState({ lastError: localization.contactmeContentMissing } as State);
		
		this.setState({ busy: true } as State);
		
		$.ajax(configuration.contactMe, {
			cache: false,
			method: "POST",
			dataType: "text",
			data: {
				subject: subject,
				message: message
			},
			error: () => {
				this.setState({ busy: false } as State);
			},
			success: data => {
				this.setState({ busy: false, completed: true } as State);
			}
		});
	}
}
