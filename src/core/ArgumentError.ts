
export default class ArgumentError extends Error {

	public arg: string;

	constructor(arg: string, message?: string) {
		super(message);

		this.arg = arg;
	}
}
