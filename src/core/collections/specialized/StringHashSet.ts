import IEnumerable from "../IEnumerable";
import ICollection from "../ICollection";
import ISet from "../ISet";

/**
 * This set is optimized for fast string access.
 */
export default class StringHashSet implements ISet<string> {

	private items: { [item: string]: void } = {};
	private itemsCount = 0;

	// --- ISet ---

	public add(item: string): boolean {
		if (this.contains(item))
			return false;
		
		this.items[item] = null;
		this.itemsCount++;
		return true;
	}

	// --- ICollection ---

	public count(): number {
		return this.itemsCount;
	}

	public isReadOnly(): boolean {
		return false;
	}

	public clear(): void {
		this.items = {};
		this.itemsCount = 0;
	}

	public contains(item: string): boolean {
		return this.items.hasOwnProperty(item);
	}

	public copyTo(array: string[], arrayIndex: number): void {
		ICollection.copyTo(this, array, arrayIndex);
	}

	public remove(item: string): boolean {
		if (!this.contains(item))
			return false;
		
		delete this.items[item];
		this.itemsCount--;
		return true;
	}

	// --- IEnumerable ---	

	public forEach(callbackfn: (value: string, index: number, enumerable: StringHashSet) => void): void {
		var i = 0;
		for (var item in this.items)
			callbackfn(item, i++, this);
	}

	public map<U>(callbackfn: (value: string, index: number, enumerable: StringHashSet) => U): IEnumerable<U> {
		return IEnumerable.map(this, callbackfn);
	}
}
