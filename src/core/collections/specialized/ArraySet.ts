import ICloneable from "../../ICloneable";
import { default as IEnumerable, IEnumerableFull } from "../IEnumerable";
import ICollection from "../ICollection";
import ISet from "../ISet";

/**
 * This set is not optimized for fast access, but is backed by a JavaScript native array.
 */
class ArraySet<T> implements ISet<T>, IEnumerableFull<T>, ICloneable {

	private items: T[] = [];
	
	constructor();
	constructor(backingArray: T[]);
	constructor(backingArray?: T[]) {
		this.items = backingArray == null ? [] : backingArray;
	}

	public getBackingArray(): T[] {
		return this.items;
	}

	// --- ISet ---

	public add(item: T): boolean {
		if (this.contains(item))
			return false;
		
		this.items.push(item);
		return true;
	}

	// --- ICollection ---

	public count(): number {
		return this.items.length;
	}

	public isReadOnly(): boolean {
		return false;
	}

	public clear(): void {
		this.items.splice(0, this.items.length);
	}

	public contains(item: T): boolean {
		return this.items.indexOf(item) >= 0;
	}

	public copyTo(array: T[], arrayIndex: number): void {
		ICollection.copyTo(this, array, arrayIndex);
	}

	public remove(item: T): boolean {
		if (!this.contains(item))
			return false;
		
		var index = this.items.indexOf(item);
		delete this.items.splice(index, 1);
		return true;
	}

	// --- IEnumerable ---	

	public forEach(callbackfn: (value: T, index: number, enumerable: ArraySet<T>) => void): void {
		this.items.forEach((value, index) => callbackfn(value, index, this));
	}

	public map<U>(callbackfn: (value: T, index: number, enumerable: ArraySet<T>) => U): IEnumerable<U> {
		return IEnumerable.map(this, callbackfn);
	}

	// --- IEnumerableFull ---

	public toArray(): T[] {
		return this.getBackingArray();
	}

	// --- ICloneable ---

	public clone(): ArraySet<T> {
		return new ArraySet(this.items.slice(0));
	}
}

export default ArraySet;
