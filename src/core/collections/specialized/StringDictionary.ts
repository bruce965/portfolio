import ArgumentError from "../../ArgumentError";
import IDictionary from "../IDictionary";
import KeyNotFoundError from "../KeyNotFoundError";

/**
 * This dictionary is optimized for fast access when using strings as keys.
 */
export default class StringDictionary<T> implements IDictionary<string, T> {

	private items: { [item: string]: T };

	constructor();
	constructor(backingObject: { [item: string]: T });
	constructor(backingObject?: { [item: string]: T }) {
		this.items = backingObject || {};
	}

	public getBackingObject(): { [item: string]: T } {
		return this.items;
	}

	// --- IDictionary ---

	public get(key: string): T {
		if (!this.containsKey(key))
			throw new KeyNotFoundError();
		
		return this.items[key];
	}

	public set(key: string, value: T): void {
		this.items[key] = value;
	}

	// TODO: getKeys(): IEnumerable<K>;
	// TODO: getValues(): IEnumerable<V>;
	
	public containsKey(key: string): boolean {
		return this.items.hasOwnProperty(key);
	}

	public add(key: string, value: T): void {
		if (this.containsKey(key))
			throw new ArgumentError('key');
		
		return this.set(key, value);
	}

	public remove(key: string): boolean {
		if (!this.containsKey(key))
			return false;
		
		delete this.items[key];
		return true;
	}

	public tryGetValue(key: string): [boolean, T] {
		if (!this.containsKey(key))
			return [false, null];
		
		return [true, this.items[key]];
	}
}
