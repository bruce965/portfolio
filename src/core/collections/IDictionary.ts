import IEnumerable from "./IEnumerable";

interface IDictionary<K, V> /* TODO: interfaces */ {

	get(key: K): V;
	set(key: K, value: V): void;
	// TODO: getKeys(): IEnumerable<K>;
	// TODO: getValues(): IEnumerable<V>;
	containsKey(key: K): boolean;
	add(key: K, value: V): void;
	remove(key: K): boolean;
	tryGetValue(key: K): [boolean, V];
}

export default IDictionary;
