import IEnumerable from "./IEnumerable";

interface ICollection<T> extends IEnumerable<T> {

	count(): number;
	isReadOnly(): boolean;

	add(item: T): void;
	clear(): void;
	contains(item: T): boolean;
	copyTo<T>(array: T[], arrayIndex: number): void;
	remove(item: T): boolean;
}

namespace ICollection {

	export function copyTo<T>(collection: ICollection<T>, array: T[], arrayIndex: number): void {
		// TODO
	}

	export function containsAny<T>(collection: ICollection<T>, items: IEnumerable<T>) {
		// TODO: break if any item is found.
		var containsAny = false;
		items.forEach(item => containsAny = containsAny || collection.contains(item));
		return containsAny;
	}

	export function containsAll<T>(collection: ICollection<T>, items: IEnumerable<T>) {
		// TODO: break if any item is found.
		var containsAll = true;
		items.forEach(item => containsAll = containsAll && collection.contains(item));
		return containsAll;
	}
}

export default ICollection;
