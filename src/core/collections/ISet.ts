import ICloneable from "../ICloneable";
import ICollection from "./ICollection";
import IEnumerable from "./IEnumerable";
import ArraySet from "./specialized/ArraySet";

interface ISet<T> extends ICollection<T> {

	add(item: T): boolean;
	// TODO
}

namespace ISet {

	export function clone<T>(set: ISet<T>): ISet<T> {
		if (ICloneable.is(set))
			// TODO: one day they'll eventually fix TypeScript compiler... replace this code then.
			return (set as any as ICloneable).clone() as any as ISet<T>;  // return set.clone() as ISet<T>;
		
		return new ArraySet(
			IEnumerable.toArray(IEnumerable.clone(this.state.selectedTools))
		);
	}
}

export default ISet;
