import ICloneable from "../ICloneable";

interface IEnumerable<T> {

	forEach(callbackfn: (value: T, index: number, enumerable: IEnumerable<T>) => void): void;
	map<U>(callbackfn: (value: T, index: number, enumerable: IEnumerable<T>) => U): IEnumerable<U>;
}

export interface IEnumerableFull<T> extends IEnumerable<T> {

	toArray?(): T[];
}

namespace IEnumerable {

	class Enumerable<I, O> implements IEnumerable<O> {

		constructor(
			private source: IEnumerable<I>,
			private converter: (value: I, index: number, enumerable: IEnumerable<I>) => O
		) { }

		public forEach(callbackfn: (value: O, index: number, enumerable: IEnumerable<O>) => void): void {
			return this.source.forEach((value, index, enumerable) => callbackfn(this.converter(value, index, enumerable), index, this));
		}

		public map<T>(callbackfn: (value: O, index: number, enumerable: IEnumerable<O>) => T): IEnumerable<T> {
			return this.source.map((value, index, enumerable) => callbackfn(this.converter(value, index, enumerable), index, this));
		}
	}

	export function map<T, U>(enumerable: IEnumerable<T>, callbackfn: (value: T, index: number, enumerable: IEnumerable<T>) => U): IEnumerable<U> {
		return new Enumerable(enumerable, callbackfn);
	}

	/**
	 * Converts an IEnumerable to array, it is not safe to assume the returned array to be a copy.
	 */
	export function toArray<T>(enumerable: IEnumerable<T>): T[] {
		if (Array.isArray(enumerable))
			return enumerable;
		
		if ((enumerable as IEnumerableFull<T>).toArray)
			return (enumerable as IEnumerableFull<T>).toArray();

		var array: T[] = [];
		enumerable.forEach(value => array.push(value));
		return array;
	}

	export function clone<T>(enumerable: IEnumerable<T>): IEnumerable<T> {
		if (ICloneable.is(enumerable))
			// TODO: one day they'll eventually fix TypeScript compiler... replace this code then.
			return (enumerable as any as ICloneable).clone() as any as IEnumerable<T>;  //return enumerable.clone() as IEnumerable<T>;
		
		var array: T[] = [];
		enumerable.forEach(value => array.push(value));
		return array;
	}
}

export default IEnumerable;
