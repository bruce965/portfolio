
interface ICloneable {

	clone(): ICloneable;
}

namespace ICloneable {

	export function is(obj: any): obj is ICloneable {
		return obj != null && typeof obj.clone == 'function';
	}
}

export default ICloneable;
