
export default class Uri {

	constructor(private uriString: string) { }

	/**
	 * WARNING: this API is not final.
	 */
	public getParameter(name: string) {
		// http://stackoverflow.com/a/901144/1135019

		name = name.replace(/[\[\]]/g, "\\$&");

		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
		var results = regex.exec(this.uriString);
		if (!results)
			return null;
		
		if (!results[2])
			return '';
		
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
}
