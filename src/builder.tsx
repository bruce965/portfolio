/// <reference path="../typings/index.d.ts" />

import * as path from "path";
import * as babel from "babel-core";
import * as childProcess from "child_process";
import * as fs from "fs";
import * as fsExtra from "fs-extra";
import * as browserify from "browserify";
const literalify = require("literalify"); //import * as literalify from "literalify";
import * as React from "react";
import * as ReactDOMServer from "react-dom/server";

import buildJSX from "./jsx/build";
import { Configuration, LocalizedPage } from "./Configuration";
import PageContents from "./PageContents";
import configuration from "./website";
import * as tools from "./tools";

function ensuredirSync(path: string): void {
	if (!fs.existsSync(path))
		fs.mkdirSync(path);
}

export default function build(): void {
	// TRANSIENT FOLDERS
	const tempFolder = path.join(process.cwd(), "temp/");
	const tsOutFolder = path.join(tempFolder, "tsout/");
	const tsxOutFolder = path.join(tempFolder, "tsxout/");

	// NAMES
	const pagesFolder = path.join(process.cwd(), "pages/");
	const dataFolder = path.join(process.cwd(), "data/");
	const templatePage = path.join(pagesFolder, "000_template.html");
	const browserBundleIndex = "src/browser.js";
	const lessInput = path.join(process.cwd(), "src/stylesheet.less");

	// FINAL FOLDERS
	const builtFolder = path.join(process.cwd(), "built/");
	const ajaxResourcesFolder = path.join(builtFolder, "ajax/");
	const browserBundle = path.join(builtFolder, "res/script.js");
	const lessOutput = path.join(builtFolder, "res/stylesheet.css");
	
	
	console.log("Building TypeScript sources...");
	childProcess.execFileSync(process.argv[0], [
		path.join(process.cwd(), "node_modules/typescript/bin/tsc"),
		"--project", process.cwd(),
		"--outDir", tsOutFolder
	]);



	console.log("Building LESS stylesheet...");
	childProcess.execFileSync(process.argv[0], [
		path.join(process.cwd(), "node_modules/less/bin/lessc"),
		lessInput,
		lessOutput
	]);



	console.log("Making client-side scripts bundle...");
	ensuredirSync(path.join(browserBundle, ".."));
	browserify()
		.add(path.join(tsOutFolder, browserBundleIndex))
		.transform(literalify.configure({
			'react': 'window.React',
			'react-dom': 'window.ReactDOM'
		}))
		.bundle()
		.pipe(fs.createWriteStream(
			browserBundle,
			{ encoding: "utf8" }
		));



	console.log("Pre-rendering pages...");
	function compileJSXPage(code: string): string {
		return babel.transform(code, { presets: ["react"] }).code;
	}
	var pageTemplate = fs.readFileSync(templatePage, "utf8")
	function renderPage(isDefault: boolean, language: string, languagesMeta: string, page: LocalizedPage, source: string, pagePath: string): string {
		var contents = buildJSX(source, isDefault, language, pagePath);
		
		var pageContents = ReactDOMServer.renderToStaticMarkup(
			<PageContents configuration={configuration} isDefault={isDefault} language={language} path={pagePath} initialContents={contents} page={page} />
		);
		
		var rendered = pageTemplate
			.replace("{{language}}", language)
			.replace("{{title}}", page.title)
			.replace("{{description}}", page.description || "")  // TODO: remove <meta> tag completely if missing
			.replace("{{languages}}", languagesMeta)
			.replace("{{javascript}}", `window.portfolioRun(
				${JSON.stringify(isDefault)},
				${JSON.stringify(language)},
				${JSON.stringify(pagePath)},
				${JSON.stringify(source)}
			);`)
			.replace("{{contents}}", pageContents);
		
		return rendered;
	}
	var pages = Configuration.allPages(configuration);
	var ajaxResources: { [path: string]: string } = {};
	for (let page of pages) {
		var languagesMeta = `<link rel="alternate" hreflang="x-default" href="${configuration.preferredScheme+"://"+configuration.domain+page.path}" />\n`;
		for (let language in page.data)
			languagesMeta += `<link rel="alternate" hreflang="${language}" href="${configuration.preferredScheme+"://"+configuration.domain+"/"+language+page.path}" />\n`;
		
		let defaultPage = true;
		
		for (let language in page.data) {
			let data = page.data[language];
			
			var source = compileJSXPage(fs.readFileSync(path.join(pagesFolder, data.contents), "utf8"));
			
			if (defaultPage) {
				let rendered = renderPage(true, language, languagesMeta, data, source, page.path);
				let folder = path.join(builtFolder, page.path);
				ensuredirSync(folder);
				fs.writeFileSync(path.join(folder, "index.html"), rendered, { encoding: "utf8" });
				defaultPage = false;
			}
			
			let rendered = renderPage(false, language, languagesMeta, data, source, page.path);
			let folder = path.join(builtFolder, language, page.path);
			ensuredirSync(folder);
			fs.writeFileSync(path.join(folder, "index.html"), rendered, { encoding: "utf8" });
			
			if (!ajaxResources[data.contents])
				ajaxResources[data.contents] = source;
		}
		
		console.log("  rendered '" + page.path + "'...");
	}



	console.log("Copying AJAX resources...");
	ensuredirSync(ajaxResourcesFolder);
	for (let resPath in ajaxResources) {
		ensuredirSync(path.join(ajaxResourcesFolder, resPath, ".."));
		fsExtra.writeFileSync(path.join(ajaxResourcesFolder, resPath), ajaxResources[resPath], "utf8");
	}


	console.log("Copying static data to destination...");
	fsExtra.copySync(dataFolder, builtFolder);
}
