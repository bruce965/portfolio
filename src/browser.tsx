/// <reference path="../typings/index.d.ts" />

import * as React from "react";
import * as ReactDOM from "react-dom";

import { Page, LocalizedPage, Configuration } from "./Configuration";
import PageContents from "./PageContents";
import configuration from "./website";
import buildJSX from "./jsx/build";
import * as tools from "./tools";

interface HistoryStateData {
	localizedPage: LocalizedPage;
	path: string;
	isDefault: boolean;
	localizedPath: string;
}

interface PageData {
	page: Page;
	language: string;
	isDefault: boolean;
}

function PageData(isDefault: boolean, language: string, page: Page): PageData {
	return { isDefault: isDefault, language: language, page: page };
}

module PageData {
	
	export function localized(data: PageData): LocalizedPage {
		if (data.isDefault)
			for (let language in data.page.data)
				return data.page.data[language];
		
		return data.page.data[data.language];
	}
	
	function localizedPath(data: PageData): string {
		if (data.isDefault)
			return data.page.path;
		
		return "/" + data.language + data.page.path;
	}
	
	export function addTo(dataByPath: { [path: string]: PageData }, page: Page): void {
		var first = true;
		for (let language in page.data) {
			if (first) {
				first = false;
				let data = PageData(true, language, page);
				dataByPath[localizedPath(data)] = data;
			}
			
			let data = PageData(false, language, page);
			dataByPath[localizedPath(data)] = data;
		}
	}
}

function portfolioRun(isDefault: boolean, language: string, path: string, contentsCode: string): void {
	var localizedPath = "/" + language + path;
	
	var allPages = Configuration.allPages(configuration);
	
	var dataByPath: { [path: string]: PageData } = {};
	for (let page of allPages)
		PageData.addTo(dataByPath, page);
	
	var initialContents = buildJSX(contentsCode, isDefault, language, path);
	
	var contentsElement = document.getElementById('contents');
	function render(isDefault: boolean, language: string, path: string, localizedPath: string, pushHistory: boolean): void {
		var localizedPage = PageData.localized(dataByPath[localizedPath]);
		
		if (pushHistory)
			history.pushState({ localizedPage: localizedPage, path: path, isDefault: isDefault, localizedPath: localizedPath } as HistoryStateData, localizedPage.title, localizedPath);
		
		ReactDOM.render(
			<PageContents configuration={configuration} isDefault={isDefault} language={language} path={path} initialContents={initialContents} page={localizedPage} />,
			contentsElement
		);
		
		//console.log("Loading page:", localizedPageByPath[path].contents, null);
	}
	
	render(isDefault, language, path, localizedPath, false);
	
	if (history.pushState) {
		// If `history.pushState` is defined, then "Single Page Application" page loading is supported.
		var currentPath = path;
		var currentIsDefault = isDefault;
		
		var origin = document.location.protocol + "//" + document.location.host;
		$(document.body).delegate('a', 'click', e => {
			var target = e.target as HTMLAnchorElement;
			var href = target.href;
			
			if (href.indexOf(origin) == 0) {
				let pathname = href.substr(origin.length);
				let data = dataByPath[pathname];
				//let localizedPage = localizedPageByPath[pathname];
				//let page = pageByPath[pathname];
				
				if (data) {
					if (currentPath != data.page.path) {
						currentPath = data.page.path;
						currentIsDefault = data.isDefault;
						render(currentIsDefault, language, currentPath, pathname, true);
					}
					
					e.preventDefault();
				}
			}
		});
		
		$(window).on('popstate', e => {
			var event = e.originalEvent as PopStateEvent;
			var state = event.state as HistoryStateData;
			
			// NOTE: `state` is null if we visit the real (non-AJAX-loaded) page.
			var newPath = state ? state.path : path;
			var newIsDefault = state ? state.isDefault : isDefault;
			var newLocalizedPath = state ? state.localizedPath : localizedPath;
			if (currentPath != newPath) {
				currentPath = newPath;
				currentIsDefault = newIsDefault;
				render(currentIsDefault, language, currentPath, newLocalizedPath, false);
			}
		});
	}
}

(window as any).portfolioRun = portfolioRun;
