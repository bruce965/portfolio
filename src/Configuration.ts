/// <reference path="../typings/index.d.ts" />

import { Date, DateRange } from "./Date";
import * as tools from "./tools";

export interface LocalizedPage {
	title: string;
	titleShort?: string;
	description?: string;
	contents: string;
}

export interface Page {
	/** Path of the page (URL format). */
	path: string;
	/** Localized content of the page. */
	data: { [language: string]: LocalizedPage };
}

export enum ProjectState {
	draft,
	inProgress,
	halted,
	completed,
	discontinued
}

export interface Project extends Page {
	tools: string[];
	platforms: string[]
	date: Date|DateRange;
	state: ProjectState;
}

export interface Tool {
	name: string;
	icon: string;
	link: string;
}

export interface Configuration {
	preferredScheme: string;
	domain: string;
	
	/** URL for "contact me" form, called through POST with parameters "subject" and "message". */
	contactMe: string;
	
	tools: { [key: string]: Tool };

	localization: { [language: string]: {
		projectName: string;
		projectDescription: string;
		projectTools: string;
		projectPlatforms: string;
		projectDate: string;
		projectState: string;
		contactmeSubject: string,
		contactmeMessage: string,
		contactmeSuccess: string,
		contactmeContentMissing: string,
		contactmeAjaxError: string
	} },
	
	/** List of pages to be accessible from main menu. */
	menu: Page[];
	/** List of projects. */
	projects: Project[];
	/** Other pages to be rendered. */
	others: Page[];
}

export module Configuration {
	
	export function allPages(configuration: Configuration): Page[] {
		return tools.distinct(
			configuration.menu,
			configuration.projects,
			configuration.others
		);
	}
}
