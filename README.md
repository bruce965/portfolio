Fabio Iotti's Portfolio
=======================

This is the source code of my portfolio generator.

This project provides tools to automatically convert a set of pages into a
multilingual browsable collection.

## Usage ##

```
# install Git, Node.js and Node Package Manager
sudo apt-get install git nodejs npm

# Clone Git project
git clone https://bruce965@bitbucket.org/bruce965/portfolio.git

cd portfolio

# Install all project dependencies
npm install

# Compile portfolio
node .
```

Output is placed in `built` folder. The contents of this folder can be uploaded
without further modifications to the root of a static web hosting.

## Customizing ##

* `pages` folder contains JSX templates for website pages.
* `pages/000_template.html` is a template used as the base for all pages.
* `src/jsx` folder contains sources for React components used in JSX templates.
* `src/style` folder contains LESS stylesheets.
  Please update `stylesheet.less` when adding stylesheets.
* `website.ts` file contains the structure of the website.
* `PageContents.tsx` contains the main React component included in all webpages.

All other files are used in the build process and do not require modifications
to customize the output website.

## License ##

You can copy, modify and redistribute this software under the terms of the MIT
license; you can redistribute the output of this compiler under your terms,
provided that you first replace all files in `pages` folder with your own.

## Contacts ##

You can contact me at [fabiogiopla@gmail.com](mailto:fabiogiopla@gmail.com) if
you need more informations about me or my projects.

Thanks for your interest,

Fabio Iotti.
