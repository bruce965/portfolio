﻿<div>
	<p>This is the content of the home page.</p>

	<p>
		This is my portfolio, it's still a work-in-progress, I'm focusing on the
		internal engine before writing actual page contents.
	</p>
	
	<p>
		Feel free to have a look around, but please don't expect to find any
		relevant information at the moment.
	</p>
</div>