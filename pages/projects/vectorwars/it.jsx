<div>
	<p>Dallo spazio profondo arrivano delle figure geometriche maligne, distruggile puntandole
	con il tuo potente cannone e sparando... Ma attenzione a non farti toccare o ti faranno male!</p>

	<p>Dopo aver imparato il già obsoleto ActionScript 2 ed aver pubblicato il mio primo gioco
	Flash: <a href="/projects/greenavoid/">Green Avoid</a>, ho imparato ActionScript 3 e pubblicato
	il mio secondo gioco Flash: Vector Wars.
	Dato che la piattaforma <a href="//wikipedia.org/wiki/Mochi_Media">Mochi Media</a> ha chiuso i battenti
	e spento i server, tentare di pubblicare (automaticamente) il punteggio a volte blocca il gioco.
	Questo gioco non ha avuto particolare successo con poco più di 60mila partite.</p>

	<KeepRatio ratio={4 / 3} style={{ width: '100%', maxWidth: 640, margin: 'auto' }}>
		<FlashMovie src="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/vectorwars/vectorwars.swf" style={{ width: '100%', height: '100%' }} />
	</KeepRatio>
</div>