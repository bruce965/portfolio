<div>
	<p>From deep space are coming evil geometric shapes, destroy them all with your
	powerful cannot... But don't let them touch you or it will hurt!</p>

	<p>After learning the already obsolete ActionScript 2 language and publishing my first Flash
	game: <a href="/projects/greenavoid/">Green Avoid</a>, I learned ActionScript 3 and published
	my second flash game: Vector Wars.
	Because the <a href="//wikipedia.org/wiki/Mochi_Media">Mochi Media</a> platform closed and
	their services shut down, trying to (automatically) post your score will eventually freeze the game.
	This game wasn't particularly successful with about 60K plays.</p>

	<KeepRatio ratio={4 / 3} style={{ width: '100%', maxWidth: 640, margin: 'auto' }}>
		<FlashMovie src="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/vectorwars/vectorwars.swf" style={{ width: '100%', height: '100%' }} />
	</KeepRatio>
</div>