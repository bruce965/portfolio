<div>
	<p>QuickStream allows to stream any multimedia file on the net!</p>

	<p>This program allows to share one file on your network, allowing other devices to play it.</p>

	<p>This tool can also be used to playback in-progress/uncompleted downloads.</p>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/quickstream/screen1.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/quickstream/screen2.png" />
	</Gallery>

	<p>Latest version: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/quickstream/QuickStream.jar">Download</a></p>
</div>