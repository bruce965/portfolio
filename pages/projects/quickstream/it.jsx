<div>
	<p>QuickStream permette di streamare sulla rete qualsiasi genere di file multimediale!</p>

	<p>Il programma si può utilizzare per condividere sulla rete locale un file multimediale,
	quindi permettere ad altri dispositivi sulla rete di visualizzarlo.</p>

	<p>Si può utilizzare questo programma anche per riprodurre film in fase di download,
	che ancora non sono stati scaricati completamente.</p>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/quickstream/screen1.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/quickstream/screen2.png" />
	</Gallery>

	<p>Ultima versione: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/quickstream/QuickStream.jar">Scarica</a></p>
</div>