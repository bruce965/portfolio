<div>
	<p>SandBox Wars 3D is the game I have been writing for my whole life.
	I changed some of the ideas between an iteration and the other, but the concept
	always remained the same.</p>

	<p>I tried implementing SandBox Wards 3D in various languages and engines, I even
	wrote a few libraries to do stuff for this game, including a mid-level network library,
	a math library and a C# Linux (Mono) port of Lidgren.</p>

	<p>
		Here are a few random documents I've written in the past about this game, please read
		first chapter of "Game Concept" to get an idea about this project.<br />
		Documents: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/docs/game_concept.pdf">Game
		Concept</a>, <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/docs/socket_protocol.pdf">Socket
		Protocol</a>, <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/docs/game_draft.pdf">Game
		Draft</a>.
	</p>

	<p>Here's a link to the <a href="//bitbucket.org/sandwar">official GIT repositories</a>; game server and database driver
	are private, everything else (including client) is public and open-source.</p>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/407053_1847647166544_1054161199_n.jpg">
			Edge of a cliff in one of the early test implementations using heightmaps instead of voxels.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/408991_1847649446601_219727286_n.jpg">
			Mountains in one of the early test implementations using heightmaps instead of voxels.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/418670_1847646126518_561071801_n.jpg">
			View from Unity editor of one of the early test implementations using heightmaps instead of voxels.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/431147_1847644606480_1802560655_n.jpg">
			Test implementations using heightmaps instead of voxels.<br />
			Chunk border normals are wrong, therefore light behaves incorrectly.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/53607_2378604480145_177746763_o.jpg">
			Screenshot from "Voxel Landscape", a cubic voxel editor I made for SandBox Wars 3D.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/74805_2390718502988_1211589233_n.jpg">
			Cubic voxel editor implemented in Unity, light occlusion test.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/188412_2487626685632_841688556_n.jpg">
			Before learning of the existence of better algorithms (name <a href="//wikipedia.org/wiki/Marching_cubes">Marching Cubes</a>),
			I made my own implementation of a smooth voxels rendering system.<br />
			My first and only implementation glitched on chunk edges; light is also glitchy.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/220666_2362290592308_1935224041_o.jpg">
			Screenshot from "Voxel Landscape", a cubic voxel editor I made for SandBox Wars 3D.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/620818_2361135083421_1958692800_o.jpg">
			Screenshot from "Voxel Landscape", a cubic voxel editor I made for SandBox Wars 3D.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/474752_2003112853089_1385475156_o.jpg">
			I even tried implementing SandBox Wars 3D in C++ with <a href="//irrlicht.sourceforge.net">Irrlicht</a>; this is an heightmap + light test.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/976875_3071449120828_1519876657_o.jpg">
			Early Unity networking test, I used <a href="//github.com/lidgren/lidgren-network-gen3">Lidgren Network</a> library.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/1008395_3087971413875_2052782524_o.jpg">
			Early Unity networking test, the server is implemented in plain C# without Unity.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/1015083_3071449320833_876165835_o.jpg" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/1015486_3123478541531_1369735458_o.jpg">
			World generation test for SandBox Wars 3D.<br />
			This algorithm generates (unrealistic) rivers, mountains, islands, lakes and oceans.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/1025531_3090346393248_76553398_o.jpg">
			Networking test: two clients connected to a common server.
		</Gallery.Item>
	</Gallery>
</div>