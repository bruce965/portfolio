<div>
	<p>SandBox Wars 3D è il videogioco che ho scritto per l'intera vita.
	Ho cambiato alcuni dei concetti tra un'iterazione e l'altra, ma l'idea di fondo
	è sempre rimasta invariata.</p>

	<p>Ho provato ad implementare SandBox Wards 3D in vari linguaggi e motori, ho anche
	varie librerie necessarie per lo sviluppo di questo gioco, inclusa una libreria di networking
	a medio livello, una libreria di matematica, ed un porting di Lidgren in C# su Linux (Mono).</p>

	<p>
		Qui sono linkati alcuni documenti (in lingua inglese) più o meno a caso che ho scritto in passato
		riguardo a questo gioco, il primo capitolo di "Game Concept" spiega l'idea originale di base
		di questo progetto.<br />
		Documenti: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/docs/game_concept.pdf">Game
		Concept</a>, <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/docs/socket_protocol.pdf">Socket
		Protocol</a>, <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/docs/game_draft.pdf">Game
		Draft</a>.
	</p>

	<p>Questo è invece un link ai <a href="//bitbucket.org/sandwar">repository GIT ufficiali</a>; il server ed i driver per
	il database sono privati, tutto il resto (incluso il client) è pubblico ed open-source.</p>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/407053_1847647166544_1054161199_n.jpg">
			Orlo di un precipizio in un test di una delle prime implementazioni, utilizza heightmap anzicché voxel.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/408991_1847649446601_219727286_n.jpg">
			Montagne in un test di una delle prime implementazioni, utilizza heightmap anzicché voxel.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/418670_1847646126518_561071801_n.jpg">
			Vista da Unity in un test di una delle prime implementazioni, utilizza heightmap anzicché voxel.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/431147_1847644606480_1802560655_n.jpg">
			Test di una delle prime implementazioni, utilizza heightmap anzicché voxel.<br />
			Le normali sono sbagliate sui bordi dei chunk, la luce si comporta quindi non modo errato.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/53607_2378604480145_177746763_o.jpg">
			Screenshot da "Voxel Landscape", un editor di voxel non-organico che ho scritto per SandBox Wars 3D.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/74805_2390718502988_1211589233_n.jpg">
			Editor di voxel non-organico che ho scritto in Unity, test della luce per occlusione.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/188412_2487626685632_841688556_n.jpg">
			Prima di scoprire l'esistenza di algoritmi migliori (come il <a href="//wikipedia.org/wiki/Marching_cubes">Marching Cubes</a>),
			Ho scritto la mia implementazione di un sistema per renderizzare voxel organici.<br />
			La mia prima ed unica implementazione aveva problemi sugli orli dei chunk; anche la luce presentava alcuni problemi.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/220666_2362290592308_1935224041_o.jpg">
			Screenshot da "Voxel Landscape", un editor di voxel non-organico che ho scritto per SandBox Wars 3D.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/620818_2361135083421_1958692800_o.jpg">
			Screenshot da "Voxel Landscape", un editor di voxel non-organico che ho scritto per SandBox Wars 3D.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/474752_2003112853089_1385475156_o.jpg">
			Ho provato anche ad implementare SandBox Wars 3D in C++ con <a href="//irrlicht.sourceforge.net">Irrlicht</a>; questo è un test di heightmap + luci.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/976875_3071449120828_1519876657_o.jpg">
			Uno dei primi test di networking con Unity, ho usato la libreria <a href="//github.com/lidgren/lidgren-network-gen3">Lidgren Network</a>.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/1008395_3087971413875_2052782524_o.jpg">
			Uno dei primi test di networking con Unity, il server è scritto in C# senza Unity.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/1015083_3071449320833_876165835_o.jpg" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/1015486_3123478541531_1369735458_o.jpg">
			Test di generazione di un mondo per SandBox Wars 3D.<br />
			Questo algoritmo genera fiumi (non realistici), montagne, isole, laghi ed oceani.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw3d/1025531_3090346393248_76553398_o.jpg">
			Test di networking: due client collegati ad un server comune.
		</Gallery.Item>
	</Gallery>
</div>