<div>
	<p>SandBox Wars 2D is a third person shooter set in the desert, the map is endless in all directions.
	Build your base and assault other players, or kill as many people as possible and use their money
	to buy powerful weapons!</p>

	<p>We began to study Java at school, it's then that I decided to try and make a videogame in
	this language as well. Java immediately felt way better than C or C++, I learnt to use
	sockets and wrote SandBox Wars 2D: my first Java game.</p>
	
	<p>The stable version doesn't include buildings. Beta version might crash when a client disconnects
	improperly, but includes many advanced features, including a YAML configuration system that allows, among
	other things, to define custom weapons, custom buildings/structures and custom bullet types. Beta version
	also allows to save and load maps and to persist game states between sessions.</p>

	<p>I wanted to also include vehicles, but abandoned the project before.</p>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/screen1.jpg">
			Old screenshot published on my first website.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/1008395_3087971413875_2052782524_o.jpg">
			Old screenshot from one of the first game versions, published on Facebook.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/394206_1700095917855_1414439733_n.jpg">
			Old screenshot from one of the first game versions, published on Facebook.
			This is a networking test.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/180884_1279750529483_6745914_n.jpg">
			Screenshot from one of the very first SandBox Wars 2D versions, written in C before learning Java.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/333638_1520916558483_1246915328_o.jpg">
			Test before knowing of Java, Flash SandBox Wars 2D version with C++ server.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/322287_1701839801451_1905520965_o.jpg">
			Test from one of the first SandBox Wars 2D Java versions.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/2016-06-07+18_57_23-Greenshot.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/2016-06-07+18_58_23-SandBox+Wars+2D+-+Server+Output.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/2016-06-07+18_56_34-SandBox+Wars+2D+-+Fabio+Iotti.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/2016-06-07+19_00_18-SandBox+Wars+2D+-+Fabio+Iotti.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/2016-06-07+19_03_36-SandBox+Wars+2D+-+Fabio+Iotti.png" />
	</Gallery>

	<p>Latest stable Windows version: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/sbw2d.zip">Download</a></p>
	<p>Latest Windows version: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/sbw2d_beta.zip">Download</a></p>
</div>