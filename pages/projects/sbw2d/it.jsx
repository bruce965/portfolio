<div>
	<p>SandBox Wars 2D è uno sparatutto in terza persona ambientato nel deserto, la mappa è illimitata in tutte le direzioni.
	Costruisci la tua base ed assalta quelle degli altri giocatori, oppure massacra più gente
	possibile e compra armi potentissime con i loro soldi!</p>

	<p>Quando a scuola cominciammo a studiare il Java, decidetti di provare a fare qualche videogioco
	anche in questo linguaggio. Decisamente mi trovai meglio con Java che con C o C++, imparai ad
	usare i socket e scrivetti SandBox Wars 2D: il mio primo gioco in Java.</p>
	
	<p>La versione stabile non include costruzioni. La versione beta a volte crasha quando un client si
	disconnette forzatamente, ma include varie funzionalità avanzate tra cui un sistema di configurazione
	in YAML che consente, tra le varie cose, di definire armi personalizzate, strutture personalizzate e
	proiettili personalizzati. La versione beta permette anche di salvare e caricare mappe e di rendere
	persistente una sessione di gioco anche quando il server viene chiuso.</p>

	<p>Volevo anche aggiungere i veicoli, ma ho abbandolato il progetto prima di riuscire.</p>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/screen1.jpg">
			Screenshot storico pubblicato sul primo sito web.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/1008395_3087971413875_2052782524_o.jpg">
			Screenshot storico di una delle prime versioni, pubblicato su Facebook.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/394206_1700095917855_1414439733_n.jpg">
			Screenshot storico di una delle prime versioni, pubblicato su Facebook.
			Questo è un test del networking.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/180884_1279750529483_6745914_n.jpg">
			Una delle primissime versioni di SandBox Wars 2D, scritto in C prima di conoscere il Java.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/333638_1520916558483_1246915328_o.jpg">
			Prova realizzata prima di conoscere Java, versione Flash di SandBox Wars 2D con server in C++.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/322287_1701839801451_1905520965_o.jpg">
			Test di una delle prime versioni in Java di SandBox Wars 2D.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/2016-06-07+18_57_23-Greenshot.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/2016-06-07+18_58_23-SandBox+Wars+2D+-+Server+Output.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/2016-06-07+18_56_34-SandBox+Wars+2D+-+Fabio+Iotti.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/2016-06-07+19_00_18-SandBox+Wars+2D+-+Fabio+Iotti.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/2016-06-07+19_03_36-SandBox+Wars+2D+-+Fabio+Iotti.png" />
	</Gallery>

	<p>Ultima versione stabile per Windows: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/sbw2d.zip">Scarica</a></p>
	<p>Ultima versione per Windows: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d/sbw2d_beta.zip">Scarica</a></p>
</div>