<div>
	<p>Reach the top of the tower without falling, contains obstacles such as invisible walls,
	bumpers, lifts and conveyors; there is only one level in this game.</p>

	<p>This is the first graphical game I ever made, the idea is simple,
	but the game is stable (doesn't contain any obvious bug).</p>

	<p>I didn't have a clue what programming could be and I never even thought I could make videogames.
	I used to buy a monthly computer magazine with softwares CD attached, probably designed for people
	lacking an internet connection like me. 
	An issue of this magazine contained a software called <a href="//blender.org">Blender</a>, this
	magazine proposed it as a software to "make 3D models and videogames". I had difficulties at the
	beginning, as Blender required a three-buttons mouse, which I didn't have.
	A few years later I found that CD again, I could the finally use Blender; I tried it and understood
	it's working. Blender is not just a powerful modeling tool, but an open-source Game Engine as well.
	Open Lab is my first graphical interactive product, after some DHTML pages.</p>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/openlab/screen1.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/openlab/screen2.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/openlab/screen3.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/openlab/screen4.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/openlab/screen5.png" />
	</Gallery>

	<p>Latest Windows version: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/openlab/openlab.7z">Download</a></p>
</div>