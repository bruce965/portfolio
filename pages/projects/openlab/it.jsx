<div>
	<p>Raggiungi la cima della torre senza cadere, comprende ostacoli come muri invisibili,
	respingenti, ascensori e nastri trasportatori; il gioco comprende un solo livello.</p>

	<p>Questo è il primo gioco grafico che ho creato in assoluto, l'idea è molto semplice,
	ma il gioco è stabile (quindi non presenta errori evidenti).</p>

	<p>Al tempo non avevo idea di cosa fosse la programmazione e non avevo mai nemmeno pensato
	di fare videogiochi. Acquistavo regolarmente un mensile di informatica con allegato un CD di
	software, probabilmente pensato per la gente che, come me, non aveva una connessione ad internet.
	In un'uscita ho trovato un programma di modellazione chiamato <a href="//blender.org">Blender</a>,
	la rivista lo proponeva come un programma per "fare modelli 3D e videogiochi". Ho inizialmente
	avuto grosse difficoltà, in quanto Blender richiedeva un mouse a tre pulsanti, che io non avevo.
	Qualche anno più tardi ho voluto ripassare in rassegna i vecchi CD di questa rivista, riscoprendo
	Blender; l'ho provato ed ho capito come funzionasse. Blender non è solo un potente strumento di
	modellazione, ma anche un Game Engine completamente open-source. <i>Open Lab</i> è il mio primo
	prodotto interattivo con grafica, dopo qualche pagina DHTML.</p>
	
	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/openlab/screen1.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/openlab/screen2.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/openlab/screen3.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/openlab/screen4.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/openlab/screen5.png" />
	</Gallery>

	<p>Ultima versione per Windows: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/openlab/openlab.7z">Scarica</a></p>
</div>