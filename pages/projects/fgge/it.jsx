<div>
	<p>Dopo aver provato disparati linguaggi di programmazione ed aver fatto disparati
	videogiochi, come prima o poi capita a tutti i ragazzini interessati alla programmazione,
	ho avuto l'idea di fare il mio game-engine.</p>

	<p>Ho rilasciato FGGE (FabioGiopla Game Engine) come una libreria per fare videogiochi 2D per browser e
	cellulari. La libreria include strimenti per riprodurre audio, strumenti per creare semplici GUI, un motore
	basato su sprite e un gestore di risorse; il pacchetto include anche qualche esempio con cui partire.</p>

	<p>Nota: non prendere questo progetto troppo sul serio, l'ho fatto quando avevo 17 anni. Senza
	dubbio funziona, ma non ho intenzione di mantenerlo e se sei interessato ad usarlo, probabilmente
	sei anche in grado di fare di meglio con i nuovi linguaggi e tecnologie; su internet si trovano
	anche varie alternative sviluppate seriamente.</p>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/2016-06-08+17_12_19-Basic+Example+-+FGGE.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/2016-06-08+17_12_38-Candle+Demo+-+FGGE.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/2016-06-08+17_13_32-Paint+-+FGGE.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/2016-06-08+17_14_11-Ping-Pong+-+FGGE.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/2016-06-08+17_14_58-Simple+GUI+-+FGGE.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/S60608-171847.jpg" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/screen1.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/screen3.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/website_it.png" />
	</Gallery>

	<p>Ultima versione: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/fgge_v07.zip">Scarica</a></p>
</div>