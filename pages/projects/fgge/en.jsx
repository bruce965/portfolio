<div>
	<p>After trying many programming languages and coding many videogames, I too, as every teenager
	interested in programming does, decided to make my own game-engine.</p>

	<p>I released FGGE (FabioGiopla Game Engine) as a library to make 2D games for browsers and
	smartphones. The library includes audio utilities, simple GUI utilities, a sprite-engine and
	a resource loader; the bundle includes some examples to get started.</p>

	<p>Note: do not take this project too seriously, I coded it when I was 17. It does indeed work,
	but I won't mantain it anymore and if you want to use it, you can probably do better with new
	programming languages and technologies; also you can find some serious alternatives on the internet.</p>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/2016-06-08+17_12_19-Basic+Example+-+FGGE.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/2016-06-08+17_12_38-Candle+Demo+-+FGGE.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/2016-06-08+17_13_32-Paint+-+FGGE.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/2016-06-08+17_14_11-Ping-Pong+-+FGGE.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/2016-06-08+17_14_58-Simple+GUI+-+FGGE.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/S60608-171847.jpg" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/screen1.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/screen3.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/website_en.png" />
	</Gallery>

	<p>Latest version: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgge/fgge_v07.zip">Download</a></p>
</div>