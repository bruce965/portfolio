<div>
	<p>Costruisci un impero di torri per difendere la tua base dall'attacco dei nemici.</p>

	<p>Attenzione: questo progetto non è mai stato concluso, potrebbe dunque presentare errori!</p>

	<p>Ho sviluppato questo gioco insieme ad un mio compagno di classe delle superiori, basato
	su un idea che ho avuto con un amico qualche anno prima. La prima versione di questo
	gioco è stata realizzata in Flash, ma non ha mai raggiunto lo stato di giocabile; questa
	versione contiene una sola mappa giocabile, ma evidentemente non è bilanciata abbastanza
	bene da risultare divertente.</p>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/element-real-defense/screen1.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/element-real-defense/screen2.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/element-real-defense/screen1.jpg" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/element-real-defense/471549_1962456556707_64815043_o.jpg">
			Screenshot dall'editor delle mappe (strumento interno non pubblicato).
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/element-real-defense/showcase.png" />
	</Gallery>

	<p>Ultima versione per Windows: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/element-real-defense/erd.zip">Scarica</a></p>
</div>