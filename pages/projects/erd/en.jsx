<div>
	<p>Build a towers empire to defend your base from the invaders.</p>

	<p>Note: this game has never been finished, mey be buggy!</p>

	<p>I developed this game with a high school classmate, based on an idea I had with a friend
	a few years before. First version of this game was in Flash, but that version never reached a
	playable state; this version contains a single playable map, clearly not balanced well enough
	to be fun to play.</p>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/element-real-defense/screen1.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/element-real-defense/screen2.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/element-real-defense/screen1.jpg" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/element-real-defense/471549_1962456556707_64815043_o.jpg">
			Screenshot from the map-editor (internal tool, not published).
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/element-real-defense/showcase.png" />
	</Gallery>

	<p>Latest Windows version: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/element-real-defense/erd.zip">Download</a></p>
</div>