<div>
	<p>
		<b>Controls</b><br />
		<br />
		<code>W S A D</code> = move the ship<br />
		<code>Mouse</code> = aim<br />
		<code>Q</code> = switch weapon<br />
		<code>Click</code> = fire<br />
		<code>Space</code> = brake
	</p>

	<p>Game is in debug mode, it is not possible to be killed.</p>

	<p>Entirely an HTML5 game, after learning to code in Flash, I began to read how programmers
	on the internet hated this technology, I tried replacing Flash and developing something; this
	is my first HTML5 game.</p>

	<KeepRatio ratio={4 / 3} style={{ width: '100%', maxWidth: 640, margin: 'auto' }}>
		<iframe src="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/spacekiller/game/spaceKiller.html" style={{ width: '100%', height: '100%', border: 0 }} />
	</KeepRatio>

	<p>Latest version: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/spacekiller/spacekiller.7z">Download</a></p>
</div>