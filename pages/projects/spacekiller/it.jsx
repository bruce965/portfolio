<div>
	<p>
		<b>Controlli</b><br />
		<br />
		<code>W S A D</code> = muovi l'astronave<br />
		<code>Mouse</code> = muovi il mirino<br />
		<code>Q</code> = cambia arma<br />
		<code>Click</code> = spara<br />
		<code>Spazio</code> = frena
	</p>

	<p>Il gioco è in modalità debug, quindi non è possibile essere sconfitti.</p>

	<p>Gioco sviluppato interamente in HTML5, dopo aver imparato a sviluppare in Flash,
	ho cominciato a leggere su internet quanto i programmatori odiassero questa tecnologia,
	ho colto l'idea di ripiazzare Flash ed ho provato a sviluppare qualcosa; questo è il
	mio primo progetto in HTML5.</p>

	<KeepRatio ratio={4 / 3} style={{ width: '100%', maxWidth: 640, margin: 'auto' }}>
		<iframe src="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/spacekiller/game/spaceKiller.html" style={{ width: '100%', height: '100%', border: 0 }} />
	</KeepRatio>

	<p>Ultima versione: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/spacekiller/spacekiller.7z">Scarica</a></p>
</div>