<div>
	<p>Make spheres and defend your base; enemy spheres shall not reach your base.
	Progress in levels, but beware: enemy spheres will become stronger!
	Requires thinking and intelligence, can be completed in less than half an hour!</p>
	
	<p>So, after <a href="/projects/openlab/">OpenLab</a> came 3D Spheres Defence: my second videogame.</p>
	
	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/3dspheresdef/screen1.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/3dspheresdef/screen2.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/3dspheresdef/screen3.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/3dspheresdef/screen4.png" />
	</Gallery>

	<p>Latest Windows version: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/3dspheresdef/3dspheresdef.7z">Download</a></p>
</div>