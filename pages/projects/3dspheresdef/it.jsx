<div>
	<p>Genera delle sfere ed usale come barriera per evitare che le sfere nemiche (Blu/Viola) raggiungano la
	tua base. Avanzando nei livelli le sfere nemiche diventano sempre più forti e resistenti!
	Necessita di ingegno ed intelligenza, si completa in meno di mezz'ora!</p>
	
	<p>E dopo <a href="/projects/openlab/">OpenLab</a> venne il turno di 3D Spheres Defence: il mio secondo
	videogioco.</p>
	
	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/3dspheresdef/screen1.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/3dspheresdef/screen2.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/3dspheresdef/screen3.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/3dspheresdef/screen4.png" />
	</Gallery>

	<p>Ultima versione per Windows: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/3dspheresdef/3dspheresdef.7z">Scarica</a></p>
</div>