<div>
	<p><b>Attenzione: non installare questo software senza aver prima letto tutto il testo in questa pagina,
	questo software potrebbe provocare gravi danni se utilizzato impropriamente!</b></p>
	
	<p>Questo strumento permette di installare <b>qualsiasi programma su una chiavetta</b>...
	Potete ad esempio installarvi il vostro videogioco preferito o il vostro programma di chat...</p>

	<p>Tutti i programmi installati <b>non vanno a modificare il computer</b> su cui vengono eseguiti,
	quindi potrete <b>utilizzarli anche a scuola</b> o sul luogo di lavoro senza che nessuno si accorga di nulla.</p>

	<p>Funziona qualsiasi programma che non richieda diritti di <b>amministratore</b> per essere eseguito,
	quindi funziona con tutti i programmi di un utente medio.</p>

	<p>Richiede una chiavetta vuota da almeno 512MB, funziona su tutti i computer Windows da XP in poi.</p>

	<p>Per potere utilizzare un programma dovrete installarlo mentre siete <b>a casa</b>,
	poi funzionerà <b>ovunque</b> (Aprire PAPF, Fare click su "Run potrable..." ed aprire l'installer del programma).</p>
	
	<h2>Come installare</h2>
	<ol>
		<li>Scaricare questo file <a href="http://www.microsoft.com/en-us/download/confirmation.aspx?id=17657">http://www.microsoft.com/en-us/download/confirmation.aspx?id=17657</a> ed
		installare.<br />(in teoria basterebbe linkd.exe, ma non esiste il download singolo)</li>
		<li>Scaricare PAPF_v03 ed avviarlo, seguire le istruzioni.</li>
		<li>Ora è possibile disinstallare il primo software.</li>
	</ol>

	<p>Ho sviluppato questo software per riuscire a portarmi ovunque gli strumenti di sviluppo,
	l'ho usato parecchio mentre fequentavo le ultime classi delle superiori.<br />
	<i>Disclaimer: tecnicamente non ho sviluppato nulla, ho solo raggruppato ed adattato un set
	di strumenti e programmi per comportarsi esattamente come volevo, ma non ho scritto codice.</i></p>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/papf/PAPF_v03.PNG" />
	</Gallery>

	<p>Ultima versione per Windows: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/papf/papf_v03.exe">Scarica</a></p>
</div>