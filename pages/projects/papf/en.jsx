<div>
	<p><b>Warning: do not install this software before reading this whole page,
	this software might damage your computer if used improperly!</b></p>
	
	<p>This tool allows to install <b>any software on your pendrive</b>...
	You might for instance install your favourite videogame or chat application...</p>

	<p>All installed programs <b>will not modify the host computer</b>,
	you can thus <b>use them at school</b> or at your workplace without anyone noticing.</p>

	<p>Any program that doesn't require <b>administrator rights</b> to be executed should work,
	all average-user programs should thus work.</p>

	<p>Requires an empty pendrive with a minimum size of 512MB, works on all Windows editions since XP.</p>

	<p>To portabilize a program you should install it while <b>at home</b>,
	it should then work <b>anywhere</b> (Run PAPF, click "Run potrable..." and select program installer).</p>
	
	<h2>How to install</h2>
	<ol>
		<li>Download <a href="http://www.microsoft.com/en-us/download/confirmation.aspx?id=17657">http://www.microsoft.com/en-us/download/confirmation.aspx?id=17657</a> and
		install.<br />(linkd.exe would be enough, but I couldn't find standalone download)</li>
		<li>Download and launch PAPF_v03, follow instructions.</li>
		<li>You can now uninstall the software from step 1.</li>
	</ol>

	<p>I made this software to carry my development tools everywhere, I used it a lot on my high school final years.<br />
	<i>Disclaimer: I actually didn't develop anything, I only grouped and adapted
	a set of tools and applications to behave as I wanted, but I have written no code.</i></p>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/papf/PAPF_v03.PNG" />
	</Gallery>

	<p>Latest Windows version: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/papf/papf_v03.exe">Download</a></p>
</div>