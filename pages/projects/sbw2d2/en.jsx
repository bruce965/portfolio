<div>
	<p>SandBox Wars 2D (variant 2) is a side-view
	2D <a href="//minecraft.net">Minecraft</a>-<a href="//terraria.org">Terraria</a> hybrid.</p>

	<p>This is my second Java videogame that came out somewhat playable.</p>
	
	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d2/382003_1597474232377_1662016027_n.jpg">
			My discovery of <a href="//wikipedia.org/wiki/Perlin_noise">Perlin Noise</a>, and the idea to make SandBox Wars 2D (variant 2).
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d2/318387_1597501233052_811967262_n.jpg">
			First version.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d2/331607_2295011030361_1486992366_o.jpg">
			First version. I already used both Windows and Linux at the time.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d2/467864_2142368254387_967118963_o.jpg">
			Zoom-out view of the world generator.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d2/377276_1607846211670_453547739_n.jpg">
			AI test: red crosses behave like animals, avoid obstables and jump across gaps. Unfortuantely they lack a model.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d2/377150_1615749169239_724001530_n.jpg">
			Liquids test, auto-leveling example; black area represents cells partially occupied by liquids.
		</Gallery.Item>
	</Gallery>

	<p>Unfortunately I couldn't find any downloadable, I might have something among my backups.</p>
</div>