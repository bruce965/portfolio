<div>
	<p>SandBox Wars 2D (variante 2) è un ibrido
	tra <a href="//minecraft.net">Minecraft</a> e <a href="//terraria.org">Terraria</a> con vista laterale 2D.</p>

	<p>Questo è il mio secondo videogioco scritto in Java risultato in qualcosa di vagamente giocabile.</p>
	
	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d2/382003_1597474232377_1662016027_n.jpg">
			La mia scoperta del <a href="//wikipedia.org/wiki/Perlin_noise">Perlin Noise</a>, e l'idea di fare SandBox Wars 2D (variante 2).
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d2/318387_1597501233052_811967262_n.jpg">
			Prima versione.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d2/331607_2295011030361_1486992366_o.jpg">
			Prima versione. Al tempo usavo già sia Windows che Linux.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d2/467864_2142368254387_967118963_o.jpg">
			Zoom-out della vista sul mondo generato.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d2/377276_1607846211670_453547739_n.jpg">
			Test intelligenza artificiale: le croci rosse si comportano come animali, evitano gli ostacoli e saltano le fosse. Sfortunatamente non hanno un modello.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/sbw2d2/377150_1615749169239_724001530_n.jpg">
			Prova dei liquidi, esempio di auto-livellamento; l'area nera rappresenta celle parzialmente occupate da liquidi.
		</Gallery.Item>
	</Gallery>

	<p>Sfortunatamente non sono riuscito a trovare niente di scaricabile, potrevi avere qualcosa tra i miei backup.</p>
</div>