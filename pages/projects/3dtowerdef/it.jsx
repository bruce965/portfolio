<div>
	<p>Costruisci le torri ed evita l'arrivo dei mostri alla tua base, scegli la torre adatta alla situazione,
	combatti contro mostri, robot, missili ed altro ancora.</p>

	<p>Attenzione: Questo gioco è stato abbandonato prima di essere concluso, potrebbe presentare errori!</p>

	<p>Questo è il mio terzo videogioco.</p>
	
	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/3dtowerdef/screen1.jpg" />
	</Gallery>

	<p>Ultima versione per Windows: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/3dtowerdef/3dtowerdef.7z">Scarica</a></p>
</div>