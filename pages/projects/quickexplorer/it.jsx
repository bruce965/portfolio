<div>
	<p>QuickExplorer è un progetto che ho sviluppato per un fine scolastico,
	partito come un semplice esplora risorse, diventato un leggero software
	con le seguenti funzionalità:</p>

	<ul>
		<li>Explorer di risorse e cartelle</li>
		<li>Media Player (Formati supportati: WAV)</li>
		<li>Visualizzatore di immagini (Formati supportati: PNG, GIF, JPEG)</li>
		<li>Editor di Testo (Formati supportati: TXT, INI, LOG) (Solo ANSI)</li>
	</ul>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/quickexplorer/screen1.png" />
	</Gallery>

	<p>Ultima versione: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/quickexplorer/QuickExplorer.jar">Scarica</a></p>
</div>