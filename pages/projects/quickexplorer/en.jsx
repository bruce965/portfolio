<div>
	<p>QuickExplorer is a simple project I made for school, started as a simple
	file browser, became a light software with the following features:</p>

	<ul>
		<li>File and folders browser</li>
		<li>Media Player (Supports: WAV)</li>
		<li>Image viewer (Supports: PNG, GIF, JPEG)</li>
		<li>Text editor (Supports: TXT, INI, LOG) (ANSI only)</li>
	</ul>

	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/quickexplorer/screen1.png" />
	</Gallery>

	<p>Latest version: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/quickexplorer/QuickExplorer.jar">Download</a></p>
</div>