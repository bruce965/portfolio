<div>
	<p>First person shooter, somewhat similar to Counter Strike.</p>
	
	<p>This is my fourth videogame, after the first three (<a href="/projects/openlab/">OpenLab
	</a>, <a href="/projects/3dspheresdef/">3D Spheres Defence</a> and <a href="/projects/3dtowerdef/">3D
	Tower Defence</a>), I discovered the existence of programming code: I learnt to copy other people's
	Python code, to customize it a bit and, with extreme satisfaction, to make something vaguely more advanced.</p>
	
	<p>Unfortunately when I worked on this project, my video card couldn't render GLSL (even less I knew what it a
	shader was), so, with default Blender settings I saw everything unshaded. With a modern video card
	lights work correctly, but since I coudn't see them when I wrote the game, all the maps look "metallic" and
	obviously buggy.</p>
	
	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgassault/screen1.jpg">
			Screenshot that I managed to save from an older version, rendered without GLSL shaders.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgassault/screen1.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgassault/screen2.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgassault/screen3.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgassault/screen4.png" />
	</Gallery>

	<p>Latest Windows version: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgassault/fgassault.7z">Download</a></p>
</div>