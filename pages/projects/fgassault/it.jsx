<div>
	<p>Sparatutto in prima persona vagamente simile a Counter Strike.</p>
	
	<p>Questo è il mio quarto videogioco, dopo i primi tre (<a href="/projects/openlab/">
	OpenLab</a>, <a href="/projects/3dspheresdef/">3D Spheres Defence</a> e <a href="/projects/3dtowerdef/">3D
	Tower Defence</a>), ho scoperto dell'esistenza del codice: ho imparato a copiare il codice Python di altri,
	personalizzarlo un po' e, con estrema soddisfazione, riuscire a fare qualcosa di vagamente più avanzato.</p>
	
	<p>Purtroppo all'epoca non avevo una scheda video in grado di renderizzare GLSL (tanto meno sapevo cosa fosse
	uno shader), quindi, con le impostazioni di default di Blender vedevo tutto senza illuminazione. Con una scheda
	video moderna le luci funzionano correttamente, ma dato che quando ho scritto il gioco non le vedevo, tutte le
	scene appaiono "metallose" ed evidentemente fatte male.</p>
	
	<Gallery>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgassault/screen1.jpg">
			Screenshot che sono riuscito a recuperare da una vecchia versione, renderizzato senza shader GLSL.
		</Gallery.Item>
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgassault/screen1.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgassault/screen2.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgassault/screen3.png" />
		<Gallery.Item image="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgassault/screen4.png" />
	</Gallery>

	<p>Ultima versione per Windows: <a href="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/fgassault/fgassault.7z">Scarica</a></p>
</div>