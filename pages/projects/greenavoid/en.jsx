<div>
	<p>Avoid geometric shapes and pickup bonuses, try to get as far as possible.</p>

	<p>After beginning to tap the power of programming, I learnt to write ActionScript 2 code with a
	software named <a href="//www.sothink.com/product/swfquicker">Sothink SWF Quicker</a> (as official
	Adobe tools were way too expensive).
	I had to make various attempts before reaching an interesting result; this is the first Flash game
	I decided to publish; I published it on the <a href="//wikipedia.org/wiki/Mochi_Media">Mochi Media</a> platform,
	which closed a few years later, I din't reach minimum payout threshold before that time.
	This game wasn't particularly successful.</p>

	<KeepRatio ratio={4 / 3} style={{ width: '100%', maxWidth: 640, margin: 'auto' }}>
		<FlashMovie src="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/greenavoid/greenavoid.swf" style={{ width: '100%', height: '100%' }} />
	</KeepRatio>
</div>