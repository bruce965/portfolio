<div>
	<p>Evita le figure geometrice e raccogli i bonus, cerca di raggiungere la maggiore distanza possibile.</p>

	<p>Dopo aver cominciato a scoprire le effettive potenzialità della programmazione,
	ho imparato a scrivere codice in ActionScript 2 tramite un software chiamato <a href="//www.sothink.com/product/swfquicker">Sothink
	SWF Quicker</a> (dato che gli strumenti ufficiali della Adobe erano decisamente troppo costosi).
	Ho fatto diverse prove prima di arrivare a risultati interessanti; questo è il primo gioco in Flash
	che ho deciso di pubblicare; l'ho pubblicato su una piattaforma conosciuta come <a href="//wikipedia.org/wiki/Mochi_Media">Mochi Media</a>,
	questa piattaforma ha chiuso qualche anno più tardi, non ho fatto in tempo a raggiungere la soglia minima per ritirare i guadagni.
	Questo gioco non ha comunque avuto particolare successo.</p>

	<KeepRatio ratio={4 / 3} style={{ width: '100%', maxWidth: 640, margin: 'auto' }}>
		<FlashMovie src="//s3-eu-west-1.amazonaws.com/static.fabioiotti.com/projects/greenavoid/greenavoid.swf" style={{ width: '100%', height: '100%' }} />
	</KeepRatio>
</div>